# @copyright   Copyright DataCorp-Traffic 2018. All rights reserved
# @file        DCTAssetRegistration.py
# @author      DC R & D
# @date        Nov 2018, 1.0
# @brief       register bulk number of asset(s)

#----------------------------------------------------------------------------------------------------------------------
#Imports
from __future__ import print_function

import os
import sys
import argparse
import datetime

import itertools
import csv #csv file reader/writer
import pyrebase #authentication for firebase
import re #for checking email validation
import socket #for getting system IP address

import urllib2 #get current address
import json # get address in json formate

from firebase.firebase import FirebaseApplication
from firebase.firebase import FirebaseAuthentication

#----------------------------------------------------------------------------------------------------------------------

#Classes
class DCTAssetRegistration:
    def __init__(self, test_args=None):

        self.__title = "Asset Registration by DATACORP-TRAFFIC"
        # Application info
        self.__app_name = "Asset Registration"
        self.__app_version = "1.0"
        self.__app_build = "29112018"
        self.__app_copyright = "Copyright 2018, DataCorp-Traffic.\nAll Rights Reserved."

        self.__current_date_timestamp = datetime.datetime.now()

        #Args Parser Setup
        parser = argparse.ArgumentParser(description=self.getApplicationName(),epilog=self.getSupportInfo())

        parser.add_argument('--email', help = 'Email Id', default = '')
        parser.add_argument('--password', help = 'Password', default = '')
        parser.add_argument('--input-csv-file-name', help = 'Input CSV File Name', default = '')
        parser.add_argument('--verbose', help = 'Verbose Mode', action = 'store_true')
        parser.add_argument('--close-on-exit', help = 'Close on exit', action = 'store_true')
        parser.add_argument('--raw-file-args', help = 'Raw arguments format - File', default = '')
        self.__args = parser.parse_args(test_args)

    #Preprocess Arguments
    def preProcessArgs(self):

        #Handle Raw File options or regular options
        self.__raw_file_args = self.__args.raw_file_args
        if 0 == len(self.__raw_file_args):

            self.__email_id                      = self.__args.email_id
            self.__password                      = self.__args.password
            self.__input_csv_file_name           = self.__args.input_csv_file_name
            self.__is_verbose_mode               = self.__args.verbose
            self.__is_close_on_exit              = self.__args.close_on_exit
        else:
            #Raw File args - from YAD
            ui_args_tokens = self.__raw_file_args.split('|')

            # print (ui_args_tokens)

            self.__version                        = ui_args_tokens.pop(0)
            self.__email_id                       = ui_args_tokens.pop(0)
            self.__password                       = ui_args_tokens.pop(0)
            self.__input_csv_file_name            = ui_args_tokens.pop(0)
            self.__is_verbose_mode                = ("TRUE" == ui_args_tokens.pop(0))
            self.__is_close_on_exit               = ("TRUE" == ui_args_tokens.pop(0))

        if self.__is_verbose_mode:
            print ("Entering into Verbose mode")
        self.printArgsAndParams()

        return self.validateArgs()

    #Validate Args
    def validateArgs(self):

        #Validate Filename
        vaild_email_address_regex = re.match('\S+@\S+.com', self.__email_id) #valid regular expression for self.__email_id
        if not vaild_email_address_regex:
            error_msg = "Error 3001: Please enter valid Email ID."
            return self.showErrorDialog(error_msg)

        if len(self.__password) < 6:
            error_msg = "Error 3002: Password must be of minimum 6 characters"
            return self.showErrorDialog(error_msg)

        if not os.path.isfile(self.__input_csv_file_name):
            error_msg = "Error 3003: '{0}' didn't exists.Please choose an existing CSV file.".format(self.__input_csv_file_name)
            return self.showErrorDialog(error_msg)

        csv_file_name_ext = self.__input_csv_file_name.split('.')[-1].lower()
        if "csv" != csv_file_name_ext:
            error_msg = "Error 3004: '{0}' is not a CSV file.Please choose a CSV file to create mask.".format(self.__input_csv_file_name)
            return self.showErrorDialog(error_msg)

        return True #Validation success

    #Prepare Args
    def prepare(self):
        print ("Starting time", self.__current_date_timestamp)
        print ("Please wait. It will take several minutes...\n")

        config = {
            'apiKey': "AIzaSyAxdOB621vYDue08LTusgIkDxmjirVjvLI",
            'authDomain': "dctsiteapp.firebaseapp.com",
            'databaseURL': "https://dctsiteapp.firebaseio.com/",
            'projectId': "dctsiteapp",
            'storageBucket': "gs://dctsiteapp.appspot.com",
            'messagingSenderId': "1047535328568",

        }
        firebase = pyrebase.initialize_app(config)

        # userEmail = self.__email_id
        # userPass = self.__password

        auth = firebase.auth()

        # Log the user in
        user = auth.sign_in_with_email_and_password(self.__email_id, self.__password)

        # Get a reference to the database service
        db = firebase.database()

        csv_hearder_fields = []
        rows = []

        sys_hostname = socket.gethostname() #get system name
        sys_IPaddress = socket.gethostbyname(socket.getfqdn()) #get default ip address 127.0.0.1
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        # print("Computer Name is:" + sys_hostname)
        # print("Computer IP Address is:" + sys_IPaddress)
        # print(s.getsockname()[0])

        # print(''+self.__current_date_timestamp.strftime('%d-%m-%Y %H:%M:%S'))

        # reading csv file
        with open(self.__input_csv_file_name, 'r') as csvfile:
        	# creating a csv reader object
        	self.__csvreader = csv.reader(csvfile)

        	# extracting field names through first row
        	csv_hearder_fields = self.__csvreader.next()

        	# extracting each data row one by one
        	for row in self.__csvreader:
        		rows.append(row)

        # printing the field names
        if self.__is_verbose_mode:
            print("Total no. of rows: %d\n"%(self.__csvreader.line_num))# get total number of rows
            print('Header names are:\n' + ', '.join(field for field in csv_hearder_fields))

        self.__no_of__header = len(csv_hearder_fields)
        if self.__no_of__header == 14:

            for row in rows:
                r = [] #list to get one by one rows
                assert r != None
                for col in row:
                    r.append(str(col))

                self.__header = ['scanSerialNumber','officeLocation','assetName','assetType','serialNumber','dctID','workStation','ownerName','vendorName','warrantyDetails','warrantyExpiryDate','remarks','currentLocation','currentAddress','tableID','installDateTime','deviceID']

                # for i in range(0,16):

                tableID = db.generate_key()

                self.__results = db.child("Asset_Install").child(tableID).set({''+self.__header[0] : ''+r[0],
                                                 ''+self.__header[1] : ''+r[1],
                                                 ''+self.__header[2] : ''+r[2],
                                                 ''+self.__header[3] : ''+r[3],
                                                 ''+self.__header[4] : ''+r[4],
                                                 ''+self.__header[5] : ''+r[5],
                                                 ''+self.__header[6] : ''+r[6],
                                                 ''+self.__header[7] : ''+r[7],
                                                 ''+self.__header[8] : ''+r[8],
                                                 ''+self.__header[9] : ''+r[9],
                                                 ''+self.__header[10] : ''+r[10],
                                                 ''+self.__header[11] : ''+r[11],
                                                 ''+self.__header[12] : ''+r[12],
                                                 ''+self.__header[13] : ''+r[13],
                                                 ''+self.__header[14] : ''+tableID,
                                                 ''+self.__header[15] : ''+self.__current_date_timestamp.strftime('%d-%m-%Y %H:%M:%S'),
                                                 ''+self.__header[16] : ''+sys_hostname},user['idToken'])

                if self.__is_verbose_mode:
                    print('\n')
                    print(r)

            self.onDone()

        else:
            print("\nERROR 3005: Please import proper csv file \nYour csv file contain {} header(s)".format(self.__no_of__header))

        self.printSummary()

    #Show error - Display it on the console
    def showErrorDialog(self, error_msg):
        print ('\n', error_msg, '\n')
        return False

    #Print Args
    def printArgsAndParams(self):
        print ('\nArguments                      :', self.__args)
        print ('Email ID                         :', self.__email_id)
        print ('Password                         :', self.__password)
        print ('Input CSV File                   :', self.__input_csv_file_name)
        print ('Verbose Mode                     :', 'Yes' if self.__is_verbose_mode else 'No')
        print ('Close on Exit                    :', 'Yes' if self.__is_close_on_exit else 'Wait (No)')

    def printSummary(self):
        completed_timestamp = datetime.datetime.now()
        diff_sec = (completed_timestamp - self.__current_date_timestamp).total_seconds()

        print ("\nStatistics")
        print ("Duration    : ", "About {} minute(s) and {} second(s)Sec".format(int(diff_sec / 60), round(diff_sec % 60, 2)))

    def showBanner(self):
        print ("\n", self.__app_name, self.__app_version, "(" + self.__app_build + ")", self.__app_copyright)
        return self

    def getApplicationName(self):
        return self.__app_name

    def getSupportInfo(self):
        return "Report bugs to abc@datacorp.in.com"

    def close(self):
        if not self.__is_close_on_exit:
            raw_input("\nPress Enter to close...")

    def onDone(self):
        print ("\n{} asset(s) registered successfully.".format((self.__csvreader.line_num)-1)) #Total number of row(s) in csv and subtract 1 for header row

#----------------------------------------------------------------------------------------------------------------------
def main(args=None):
    app = DCTAssetRegistration(args)
    app.showBanner()
    app.preProcessArgs()
    app.prepare()
    sys.stdout.flush()
    sys.stderr.flush()
    app.close()
#----------------------------------------------------------------------------------------------------------------------

#Main Entry
if __name__ == '__main__':

    try:
        main()
    except Exception as ex:
        print (ex)
        raw_input('\nPress Enter to close...')
    finally:
        pass
