#!/bin/bash

echo building Asset Registration...
echo

echo Removing old compiled modules...
rm *.pyc

echo Compiling modules...
python -m compileall .

desktop-file-validate asset-registration.desktop

echo Staging files...
dpkg --remove AssetRegistration
rm -rf install
mkdir install
mkdir install/AssetRegistration

mkdir install/AssetRegistration/DEBIAN

#opt/datacorp-traffic/AssetRegistration
mkdir install/AssetRegistration/opt
mkdir install/AssetRegistration/opt/datacorp-traffic
mkdir install/AssetRegistration/opt/datacorp-traffic/AssetRegistration
mkdir install/AssetRegistration/opt/datacorp-traffic/AssetRegistration/icons

#/usr/share/applications
mkdir install/AssetRegistration/usr
mkdir install/AssetRegistration/usr/share
mkdir install/AssetRegistration/usr/share/applications

cp --verbose control install/AssetRegistration/DEBIAN
cp --verbose *.pyc install/AssetRegistration/opt/datacorp-traffic/AssetRegistration
cp --verbose *.png install/AssetRegistration/opt/datacorp-traffic/AssetRegistration
cp --verbose icons/* install/AssetRegistration/opt/datacorp-traffic/AssetRegistration/icons
cp --verbose *.desktop install/AssetRegistration/opt/datacorp-traffic/AssetRegistration
cp --verbose *.desktop install/AssetRegistration/usr/share/applications
cp --verbose run.sh install/AssetRegistration/opt/datacorp-traffic/AssetRegistration
chmod 777 install/AssetRegistration/opt/datacorp-traffic/AssetRegistration/run.sh
echo Staging files...Done

rm *.pyc
echo Cleaning complied python files...Done

echo Packing as a debian package...
cd install
dpkg-deb --build AssetRegistration
ls -l AssetRegistration.deb
dpkg --contents AssetRegistration.deb
echo Packing as a debian package...Done

echo Installing AssetRegistration...
echo
echo To Install AssetRegistration,
echo "    sudo dpkg -i `pwd`/AssetRegistration.deb"
echo To Remove AssetRegistration,
echo "    sudo dpkg --remove AssetRegistration"
echo

echo Building AssetRegistration...Done
echo

