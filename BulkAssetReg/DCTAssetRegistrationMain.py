# @copyright   Copyright DataCorp-Traffic 2018. All rights reserved
# @file        DCTAssetRegistrationMain.py
# @author      DC R & D
# @date        Nov 2018, 1.0
# @brief       register bulk number of asset(s)

#----------------------------------------------------------------------------------------------------------------------

#Imports
from __future__ import print_function

import os
import sys
import argparse
import re

#----------------------------------------------------------------------------------------------------------------------
#Classes

class DCTAssetRegistrationUI:
    def __init__(self):

        self.__title = "Asset Registration by DATACORP-TRAFFIC"
        # Application info
        self.__app_name = "Asset Registration"
        self.__app_version = "1.0"
        self.__app_build = "29112018"
        self.__app_copyright = "Copyright 2018, DataCorp-Traffic.\nAll Rights Reserved."

        #Build YAD (Yet Another Dialog) Command framgments
        self.__yad_cmd_frags = [
            'yad',
            '--center',
            '--mouse',
            '--width=550',
            '--borders=15',
            '--on-top',
            '--title="' + self.__title + '"',
            '--image="' + os.getcwd() + '/asset_ic.png"',

            '--form',
            '--output-by-row',

            '--field="  Version:RO"', '"1.0"',
            '--field="  Email ID"', '""',
            '--field="  Password:H"', '""',
            '--field="  Input CSV File:FL"', '" "',
            '--field="  Verbose :CHK"', "FALSE",
            '--field="  Close on exit :CHK"', "FALSE",
        ]

    def showErrorDialog(self, error_message, repeat_work = True):

        yad_error_dialg_frags = [
            'yad',
            '--center',
            '--mouse',
            '--width=450',
            '--borders=8',
            '--on-top',
            '--title="' + self.__title + '"',
            '--image="' + os.getcwd() + '/ast_err_ic.png"', #getcwd() - get current working directory
            '--text="' + error_message + '"',
            '--button=Ok:1'
        ]
        yad_cmd = " ".join(yad_error_dialg_frags) + " 2>/dev/null"
        # print (yad_cmd)

        result = os.popen(yad_cmd).read().strip()
        if repeat_work:
            return self.doWork()
        else:
            return result

    def run_script_in_new_console(self, script_name, script_args=""):

        #Assume Py-script
        script_file_name = os.getcwd() + '/' + script_name + '.pyc'

        if not os.path.isfile(script_file_name):
            script_file_name = os.getcwd() + '/' + script_name + '.py'

        # print ("file",script_file_name)
        #Python Exe
        sys_executable = sys.executable
        paths = os.environ['PATH'].split(':')
        anaconda2_paths = filter(lambda x: 'anaconda2/bin' in x, paths)
        if 0 != len(anaconda2_paths):
            sys_executable = os.path.join(anaconda2_paths[0], 'python')

        script_run_cmd_frags = [
            "x-terminal-emulator",
            "-e",
            sys_executable,
            script_file_name,
            script_args
        ]

        script_run_cmd = " ".join(script_run_cmd_frags)
        return os.popen(script_run_cmd)

    def doWork(self):
        #Obtain Yad Command
        yad_cmd = " ".join(self.__yad_cmd_frags) +  " 2>/dev/null"
        # print (yad_cmd)

        ui_result = os.popen(yad_cmd).read().strip()
        ui_result = ui_result[:-1]
        # print (ui_result)
        if 0 == len(ui_result):
            return False    #User cancelled

        ui_args_tokens = ui_result.split("|")

        self.__version                        = ui_args_tokens.pop(0)
        self.__email_id                       = ui_args_tokens.pop(0)
        self.__password                       = ui_args_tokens.pop(0)
        self.__input_csv_file_name            = ui_args_tokens.pop(0)
        self.__is_verbose_mode                = ui_args_tokens.pop(0)
        self.__is_close_on_exit               = ui_args_tokens.pop(0)

        print ("Version                 :", self.__version)
        print ("Email Id                :", self.__email_id)
        print ("Input CSV File          :", self.__input_csv_file_name)
        print ("Verbose Mode            :", self.__is_verbose_mode)
        print ("Close On Exit           :", self.__is_close_on_exit)

        #Validate Filename
        vaild_email_address_regex = re.match('\S+@\S+.com', self.__email_id) #valid regular expression for self.__email_id
        if not vaild_email_address_regex:
            error_msg = "Error 3001: Please enter valid Email ID."
            return self.showErrorDialog(error_msg)

        if len(self.__password) < 6:
            error_msg = "Error 3002: Password must be of minimum 6 characters"
            return self.showErrorDialog(error_msg)

        if not os.path.isfile(self.__input_csv_file_name):
            error_msg = "Error 3003: '{0}' didn't exists.\n\nPlease choose an existing CSV file.".format(self.__input_csv_file_name)
            return self.showErrorDialog(error_msg)

        csv_file_name_ext = self.__input_csv_file_name.split('.')[-1].lower()
        if "csv" != csv_file_name_ext:
            error_msg = "Error 3004: '{0}' is not a CSV file.\n\nPlease choose a CSV file to create mask.".format(self.__input_csv_file_name)
            return self.showErrorDialog(error_msg)

        raw_file_args = '--raw-file-args "' + ui_result + '"\n'
        print ("\nRaw args", raw_file_args)
        return self.run_script_in_new_console('DCTAssetRegistration', raw_file_args)

def main():
    DCTAssetRegistrationUI().doWork()

#----------------------------------------------------------------------------------------------------------------------
#Main Entry
if __name__ == "__main__":
    try:
        main()
    except Exception as ex:
        print (ex)
    finally:
        raw_input("Press Enter to close...")
