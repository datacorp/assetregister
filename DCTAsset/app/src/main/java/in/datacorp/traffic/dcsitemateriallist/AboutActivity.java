package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.annotation.SuppressLint;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.Calendar;

/**
 * AboutActivity holds information about the app details like app name, logo, version, copyright year etc.,
 */
public class AboutActivity extends AppCompatActivity {

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        /**
         * String to get Version name and Version node using gradle build
         */
        String versionName = "";
        int versionCode = -1;
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        /**
         * TextView for printing Version name & Version code
         */
        TextView textViewVersionInfo = (TextView) findViewById(R.id.textView_versionInfo);
        textViewVersionInfo.setText(String.format("Version %s \nVersion code %d", versionName, versionCode));

        /**
         * String to get CopyRight year
         */
        String cp = String.format("© %d by Datacorp Traffic pvt. ltd",Calendar.getInstance().get(Calendar.YEAR));

        /**
         * TextView for printing CopyRight year
         */
        TextView txt = (TextView) findViewById(R.id.textView_copyright);
        txt.setText(cp);

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
