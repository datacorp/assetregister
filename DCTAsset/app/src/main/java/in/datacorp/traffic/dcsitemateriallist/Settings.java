package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Setting activity contains DataCorp details like about, contact, change password
 */
public class Settings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.activity_settings);

        /**
         * Button to go About Activity
         */
        ((Button)findViewById(R.id.about)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent about = new Intent(Settings.this,AboutActivity.class);
                Settings.this.startActivity(about);
            }
        });

        /**
         * Button to go ChangePwd Activity
         */
        ((Button)findViewById(R.id.changePwd)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent setPwd = new Intent(Settings.this,NewPasswordActivity.class);
                Settings.this.startActivity(setPwd);
            }
        });

        /**
         * Button to go Contact Activity
         */
        ((Button)findViewById(R.id.contact)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent setPwd = new Intent(Settings.this,ContactActivity.class);
                Settings.this.startActivity(setPwd);
            }
        });
    }

}
