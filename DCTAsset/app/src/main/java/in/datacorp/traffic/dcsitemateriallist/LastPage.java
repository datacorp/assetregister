package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * LastPage Activity, it consists of a back and exit buttons, which is used for exit the entire app or back create new records
 */
public class LastPage extends AppCompatActivity {
    public final static String MESSAGE_KEY ="hariharan.senddata.message_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.last_page);

        /**
         * Creating UI reference
         */
        Button btn_logout = (Button) findViewById(R.id.exit);

        Button startTutorials = (Button) findViewById(R.id.back);

        /**
         * Adding an OnClickListener to btn_logout
         */
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(LastPage.this);
                builder.setTitle("Exit");
                builder.setMessage("Do you want to exit ??");
                builder.setPositiveButton("Yes. Exit now!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                           moveTaskToBack(true);//close entire app
                       // System.exit(0); //close particular page alone
                        // LastPage.super.onBackPressed();
                        finish();

                    }
                });

                builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        /**
         * Adding an OnClickListener to startTutorials
         */
        startTutorials.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tutorialsActivityIntent = new Intent(LastPage.this, IndexActivity.class);
                LastPage.this.startActivity(tutorialsActivityIntent);

            }
        });

    }

}
