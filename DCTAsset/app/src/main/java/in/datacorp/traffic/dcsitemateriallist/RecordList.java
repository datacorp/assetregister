package in.datacorp.traffic.dcsitemateriallist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import in.datacorp.traffic.dcsitemateriallist.model.Register;
import in.datacorp.traffic.dcsitemateriallist.model.Transfer;

/**
 * This is the reference class for all activity,used to show the information about asset
 */
public class RecordList extends ArrayAdapter<Register>{
    private Activity context;
    private List<Register> register_list;

    public RecordList(Activity context,List<Register> dateRecords) {
        super(context, R.layout.asset_status_list, dateRecords);
        this.context = context;
        this.register_list = dateRecords;
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.asset_status_list, null, true);

        TextView txtv_scan_serial_number = (TextView) listViewItem.findViewById(R.id.txtv_scan_serial_number);
        TextView txtv_ofc_loc = (TextView) listViewItem.findViewById(R.id.txtv_ofc_loc);
        TextView txtv_ast_owner = (TextView) listViewItem.findViewById(R.id.txtv_ast_owner);
        TextView txtv_ast_name = (TextView) listViewItem.findViewById(R.id.txtv_ast_name);
        TextView txtv_ast_type = (TextView) listViewItem.findViewById(R.id.txtv_ast_type);
        TextView txtv_ast_serial_num = (TextView) listViewItem.findViewById(R.id.txtv_ast_serial_num);
        TextView txtv_ast_dc_id = (TextView) listViewItem.findViewById(R.id.txtv_ast_dc_id);
        TextView txtv_ast_wrk_stn = (TextView) listViewItem.findViewById(R.id.txtv_ast_wrk_stn);
        TextView txtv_ast_vendor = (TextView) listViewItem.findViewById(R.id.txtv_ast_vendor);
        TextView txtv_ast_wrnty_dtils = (TextView) listViewItem.findViewById(R.id.txtv_ast_wrnty_dtils);
        TextView txtv_ast_wrnty_expiry = (TextView) listViewItem.findViewById(R.id.txtv_ast_wrnty_expiry);
        TextView txtv_remarks = (TextView) listViewItem.findViewById(R.id.txtv_remarks);

        Register reg = register_list.get(position);

        txtv_scan_serial_number.setText(reg.getscanSerialNumber());
        txtv_ofc_loc.setText(reg.getofficeLocation());
        txtv_ast_owner.setText(reg.getownerName());
        txtv_ast_name.setText(reg.getassetName());
        txtv_ast_type.setText(reg.getassetType());
        txtv_ast_serial_num.setText(reg.getserialNumber());
        txtv_ast_dc_id.setText(reg.getdctID());
        txtv_ast_wrk_stn.setText(reg.getworkStation());
        txtv_ast_vendor.setText(reg.getvendorName());
        txtv_ast_wrnty_dtils.setText(reg.getwarrantyDetails());
        txtv_ast_wrnty_expiry.setText(reg.getwarrantyExpiryDate());
        txtv_remarks.setText(reg.getremarks());

        return listViewItem;
    }
}
