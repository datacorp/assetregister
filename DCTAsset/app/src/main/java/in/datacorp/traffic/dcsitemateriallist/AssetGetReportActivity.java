package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import statements
 */
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import in.datacorp.traffic.dcsitemateriallist.model.Register;
import in.datacorp.traffic.dcsitemateriallist.model.Service;
import in.datacorp.traffic.dcsitemateriallist.model.ServiceReceived;

/**
 * AssetGetReportActivity generate entire report of the asset management
 */
public class AssetGetReportActivity extends AppCompatActivity {

    /**
     * SearchableSpinner to get asset name
     */
    SearchableSpinner asset_name_spinner;
    /**
     * Button to generate report
     */
    Button btn_getReport;
    /**
     * String for storing selected asset
     */
    String selected_asset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.asset_get_report);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /**
         * UI References
         */
        asset_name_spinner = (SearchableSpinner) findViewById(R.id.spinnerAsset);

        btn_getReport = (Button) findViewById(R.id.button_generateReport);

        /**
         * Getting File Permission to store the report
         */
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
        }

        /**
         * Generates report on button click
         */
        btn_getReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (null == asset_name_spinner.getSelectedItem()) {
                    Toast.makeText(getApplicationContext(),"Please select required document ",Toast.LENGTH_LONG).show();
                    return;
                }
                selected_asset = asset_name_spinner.getSelectedItem().toString();

                final DatabaseReference SiteResponseRef = FirebaseDatabase.getInstance().getReference(selected_asset);

                SiteResponseRef.addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        @SuppressLint("StaticFieldLeak")
                        class SiteWriterTasker extends android.os.AsyncTask<Iterable<DataSnapshot>, Integer, String> {

                            private String selected_report;

                            private SiteWriterTasker(String sel_rpt) {
                                this.selected_report = sel_rpt;
                            }

                            @SafeVarargs
                            protected final String doInBackground(Iterable<DataSnapshot>... responseSnapshotes) {

                                Iterable<DataSnapshot> responseSnapshotIter = responseSnapshotes[0];

                                try {

                                    final File root_folder = new File(Environment.getExternalStorageDirectory(),"Asset Records");
                                    if (!root_folder.exists()) {
                                        root_folder.mkdirs();
                                    }

                                    switch (selected_asset) {
                                        case "Asset_Install": {
                                            final String report_filename = "Asset_Install CreatedOn " + DateFormat.format("MM-dd-yyyy-h-mm-ss-aa",System.currentTimeMillis()).toString();
                                            final File asset_report_file_path = new File(root_folder,report_filename + ".csv");
                                            FileWriter asset_report_file_writer = new FileWriter(asset_report_file_path,true);// file path to save

                                            writeAssetIstallHeader(asset_report_file_writer,selected_report);

                                            int count = 0;
                                            publishProgress(count);
                                            for (DataSnapshot assetResponseSnapshot : responseSnapshotIter) {
                                                Register reg = assetResponseSnapshot.getValue(Register.class);
                                                assert (null != reg);
                                                writeAssetInstallResponse(asset_report_file_writer,reg);
                                                for(DataSnapshot asset_transferID : assetResponseSnapshot.getChildren()){
                                                    if(asset_transferID.getKey().equals("asset_transfer_records")) {
                                                        String transfer_list = asset_transferID.getValue().toString();
                                                        Log.d("Specific Node Value",transfer_list);
                                                        asset_report_file_writer.write(transfer_list);
                                                        asset_report_file_writer.write(",");
//                                                        asset_report_file_writer.write("\n");
                                                    }
                                                }
                                                asset_report_file_writer.write("\n");
                                                count++;
                                                publishProgress(count);
                                            }

                                            asset_report_file_writer.close();
                                            return asset_report_file_path.toString();
                                        }
                                        case "Asset_Servicing": {
                                            final String report_filename = "Asset_given_for_srvc CreatedOn " + DateFormat.format("MM-dd-yyyy-h-mm-ss-aa",System.currentTimeMillis()).toString();
                                            final File asset_report_file_path = new File(root_folder,report_filename + ".csv");
                                            FileWriter asset_report_file_writer = new FileWriter(asset_report_file_path,true);// file path to save

                                            writeAssetServiceHeader(asset_report_file_writer,selected_report);

                                            int count = 0;
                                            publishProgress(count);
                                            for (DataSnapshot assetResponseSnapshot : responseSnapshotIter) {
                                                Service service = assetResponseSnapshot.getValue(Service.class);
                                                assert (null != service);
                                                writeAssetServiceResponse(asset_report_file_writer,service);
                                                count++;
                                                publishProgress(count);
                                            }

                                            asset_report_file_writer.close();
                                            return asset_report_file_path.toString();
                                        }

                                        case "Asset_Taken_From_Service_Center": {
                                            final String report_filename = "Asset_taken_frm_srvc CreatedOn " + DateFormat.format("MM-dd-yyyy-h-mm-ss-aa",System.currentTimeMillis()).toString();
                                            final File asset_report_file_path = new File(root_folder,report_filename + ".csv");
                                            FileWriter asset_report_file_writer = new FileWriter(asset_report_file_path,true);// file path to save

                                            writeAssetTakenHeader(asset_report_file_writer,selected_report);

                                            int count = 0;
                                            publishProgress(count);
                                            for (DataSnapshot assetResponseSnapshot : responseSnapshotIter) {
                                                ServiceReceived taken = assetResponseSnapshot.getValue(ServiceReceived.class);
                                                assert (null != taken);
                                                writeAssetTakenResponse(asset_report_file_writer,taken);
                                                count++;
                                                publishProgress(count);
                                            }

                                            asset_report_file_writer.close();
                                            return asset_report_file_path.toString();
                                        }
                                    }
                                    return "File Empty";
                                } catch (IOException e) {
                                    return ""+e;
                                }
                            }

                            protected void onProgressUpdate(Integer... progress) {
                                AssetGetReportActivity.this.setTitle("Done with " + progress[0].intValue());
                            }

                            protected void onPostExecute(String result) {
                                Toast.makeText(getApplicationContext(),("Report generated " + result),Toast.LENGTH_LONG).show();
                            }
                        }

                        Toast.makeText(getApplicationContext(),"Collecting " + dataSnapshot.getChildrenCount() + " record(s) received for site install :" + selected_asset,Toast.LENGTH_LONG).show();

                        SiteWriterTasker swt = new SiteWriterTasker(selected_asset);
                        Iterable<DataSnapshot> surveyResponseSnapshotIter = dataSnapshot.getChildren();
                        swt.execute(surveyResponseSnapshotIter);

                        Toast.makeText(getApplicationContext()," record(s) received :" + selected_asset,Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getApplicationContext(),"Unable to obtain site records for :" + selected_asset,Toast.LENGTH_LONG).show();
                    }
                });

                Toast.makeText(getApplicationContext(),"Selected :" + selected_asset,Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure to cancel?")
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0,int arg1) {
                        AssetGetReportActivity.super.onBackPressed();
                        finish();
                        //close();
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0,int arg1) {

                    }
                })
                .show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /*
     * Header for asset_install report
     */
    private void writeAssetIstallHeader(FileWriter writer,String selected_asset) throws IOException {

        writer.write("QR Serial Number");
        writer.write(",");
        writer.write("Office Location");
        writer.write(",");
        writer.write("Asset Type");
        writer.write(",");
        writer.write("Date & Time");
        writer.write(",");
        writer.write("Device ID");
        writer.write(",");
        writer.write("WorkStation");
        writer.write(",");
        writer.write("Asset Name");
        writer.write(",");
        writer.write("Asset Serial Number");
        writer.write(",");
        writer.write("DataCorp Asset ID");
        writer.write(",");
        writer.write("Asset Owner");
        writer.write(",");
        writer.write("Warranty Details");
        writer.write(",");
        writer.write("Warranty Expiry Date");
        writer.write(",");
        writer.write("Vendor Name");
        writer.write(",");
        writer.write("Remarks");
        writer.write(",");
        writer.write("Latitude");
        writer.write(",");
        writer.write("Longitude");
        writer.write(",");
        writer.write("Address Line1");
        writer.write(",");
        writer.write("Line2");
        writer.write(",");
        writer.write("Line3");
        writer.write(",");
        writer.write("Line4");
        writer.write(",");
        writer.write("Line5");
        writer.write(",");
        writer.write("Line6");
        writer.write(",,,,");
        writer.write("List of New Owner(s)");

        writer.write("\r\n");
    }

    /*
     * Response for asset_install report
     */
    private void writeAssetInstallResponse(final FileWriter writer,final Register asset_response) throws IOException {

        writer.write(asset_response.getscanSerialNumber());
        writer.write(",");

//        if(asset_response.getscanSerialNumber().equals("DC/BLR/MOUSE/0104")){
//            Log.d("TableID",""+asset_response.getTableID());
//        }
        writer.write(asset_response.getofficeLocation());
        writer.write(",");
        writer.write(asset_response.getassetType());
        writer.write(",");
        writer.write(asset_response.getinstallDateTime());
        writer.write(",");
        writer.write(asset_response.getdeviceID());
        writer.write(",");
        writer.write(asset_response.getworkStation());
        writer.write(",");
        writer.write(asset_response.getassetName());
        writer.write(",");
        writer.write(asset_response.getserialNumber());
        writer.write(",");
        writer.write(asset_response.getdctID());
        writer.write(",");
        writer.write(asset_response.getownerName());
        writer.write(",");
        writer.write(asset_response.getwarrantyDetails());
        writer.write(",");
        writer.write(asset_response.getwarrantyExpiryDate());
        writer.write(",");
        writer.write(asset_response.getvendorName());
        writer.write(",");

        if (asset_response.getremarks().equals("")) {
            writer.write("No Remarks");
            writer.write(",");
        } else {
            writer.write(asset_response.getremarks());
            writer.write(",");
        }

        writer.write(asset_response.getcurrentLocation());
        writer.write(",");
        writer.write(asset_response.getcurrentAddress());
        writer.write(",");

    }

    /*
     * Asset Given for Servicing Details
     */
    private void writeAssetServiceHeader(FileWriter writer,String selected_asset) throws IOException {

        writer.write("ScanSerialNumber");
        writer.write(",");
        writer.write("DateTime");
        writer.write(",");
        writer.write("ServiceCenter");
        writer.write(",");
        writer.write("ServiceRequestNumber");
        writer.write(",");
        writer.write("Contact Person Name");
        writer.write(",");
        writer.write("Phone Number");
        writer.write(",");
        writer.write("Given Date");
        writer.write(",");
        writer.write("VehicleDetails");
        writer.write(",");
        writer.write("ProblemDescription");
        writer.write(",");
        writer.write("Remarks");
        writer.write(",");

        writer.write("\r\n");
    }

    private void writeAssetServiceResponse(final FileWriter writer,final Service asset_response) throws IOException {

        writer.write(asset_response.getscanSerialNumber());
        writer.write(",");
        writer.write(asset_response.getcurrentDateTime());
        writer.write(",");
        writer.write(asset_response.getserviceCenter());
        writer.write(",");
        writer.write(asset_response.getserviceRequestNumber());
        writer.write(",");
        writer.write(asset_response.getcontactName());
        writer.write(",");
        writer.write(asset_response.getphoneNumber());
        writer.write(",");
        writer.write(asset_response.getgivenDate());
        writer.write(",");
        writer.write(asset_response.getvehicleDetails());
        writer.write(",");
        writer.write(asset_response.getproblemDescription());
        writer.write(",");

        if (asset_response.getremarks().equals("")) {
            writer.write("No Remarks");
            writer.write(",");
        } else {
            writer.write(asset_response.getremarks());
            writer.write(",");
        }

    }

    /*
     * Asset Taken from service center Details
     */
    private void writeAssetTakenHeader(FileWriter writer,String selected_asset) throws IOException {

        writer.write("ScanSerialNumber");
        writer.write(",");
        writer.write("DateTime:");
        writer.write(",");
        writer.write("Office Location");
        writer.write(",");
        writer.write("Asset Condition");
        writer.write(",");

        writer.write("\r\n");
    }

    private void writeAssetTakenResponse(final FileWriter writer,final ServiceReceived asset_response) throws IOException {

        writer.write(asset_response.getscanSerialNumber());
        writer.write(",");
        writer.write(asset_response.getcurrentDateTime());
        writer.write(",");
        writer.write(asset_response.getofficeLocation());
        writer.write(",");
        writer.write(asset_response.getcondition());
        writer.write(",");

    }
}
