package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import in.datacorp.traffic.dcsitemateriallist.model.SendMail;
import in.datacorp.traffic.dcsitemateriallist.model.ServiceReceived;
import in.datacorp.traffic.dcsitemateriallist.model.Transfer;

/**
 * This activity used to store the details of asset received from service center
 */
public class ServiceAssetReceived extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    /**
     * List to get all the item from database
     */
    List<Transfer> item;

    /**
     * IntentIntegrator for qr scanner
     */
    private IntentIntegrator qrScan;

    /**
     * TextView to store qr scanned text
     */
    private TextView scanText;

    /**
     * Spinner to get Office Location
     */
    private Spinner spinner;

    /**
     * TableLayout display after getting scan text result
     */
    private TableLayout tbl_lyt;

    /**
     * ArrayAdapter for adapter view
     */
    ArrayAdapter adapter;

    /**
     * ArrayList to store office location
     */
    ArrayList<String> ofc_loc;

    /**
     * RadioButton to get working condition of asset
     */
    private RadioGroup conditionGroup;

    private RadioButton conditionButton;

    /**
     * RadioButton for working, not_working, scrap
     */
    RadioButton rb_wrk,rb_not_wrk,rb_scrap;

    private Boolean flash = false;

    /**
     * String of database path to store reference db
     */
    public static final String Database_Path = "Asset_Taken_From_Service_Center";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.service_asset_received);

        /**
         * Creating UI reference
         */
        tbl_lyt = (TableLayout)findViewById(R.id.table2);

        spinner = (Spinner) findViewById(R.id.spinnerGenres);

        qrScan = new IntentIntegrator(this);

        scanText = (TextView)findViewById(R.id.scanText);

        conditionGroup = (RadioGroup) findViewById(R.id.asset_condition);

        rb_wrk = (RadioButton) findViewById(R.id.working);

        rb_not_wrk =(RadioButton)findViewById(R.id.not_working);

        rb_scrap = (RadioButton) findViewById(R.id.scrap);

        /**
         * Create an ArrayAdapter using the string array and a default spinner layout
         */
        ofc_loc = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.office_location)));

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,ofc_loc);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);

        tbl_lyt.setVisibility(View.INVISIBLE);

        scanText.setVisibility(View.INVISIBLE);

        /**
         * ArrayList to store item records
         */
        item = new ArrayList<>();

        /**
         *  Creating an OnCheckedChangeListener, if radio button values are changed which send the notification to registered mailID
         */
        conditionGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                Log.d("RadioGroup",""+checkedId);
                if(checkedId==R.id.scrap){
//                  if(checkedId==2131230897){

                    final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(ServiceAssetReceived.this);
                    builder.setTitle("Warning!!");
                    builder.setMessage("Scrap notification will send to Admin|Manager");
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            sendEmail();
                        }
                    });

                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                            rb_wrk.setChecked(true);
                        }
                    });

                    android.support.v7.app.AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });


        /**
         * Adding an OnClickListener to buttonScan
         */
        ((Button) findViewById(R.id.buttonScan)).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                qrScan.initiateScan();
            }
        });

        /**
         * Adding an OnClickListener to saveItems
         */
        ((Button) findViewById(R.id.saveItems)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /**
                 * Validate Form
                 */
                if (!validateForm()) {
                    return;
                }

                Log.d("flash",""+flash);

                /**
                 * Creating DB reference for Storing the values
                 */
                DatabaseReference srvc_ast_received = FirebaseDatabase.getInstance().getReference(Database_Path);

                /**
                 * Getting the ID from firebase database.
                 */
                String srvc_asset_received_table_key = srvc_ast_received.push().getKey();

                String scan_txt = scanText.getText().toString();
                String ofcLocation = spinner.getSelectedItem().toString();
                int conditionID = conditionGroup.getCheckedRadioButtonId();
                conditionButton = (RadioButton) findViewById(conditionID);
                String conditionResult = conditionButton.getText().toString();
                Log.d("RadioResults",""+conditionResult);

                /**
                 * Set Current Date & Time as the asset received time
                 */
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
                final String ast_received_date = sdf.format(new Date());

                /**
                 * Creating data model for ServiceReceived
                 */
                ServiceReceived ast_received = new ServiceReceived();

                ast_received.setCurrentDateTime(ast_received_date);
                ast_received.setScanSerialNumber(scan_txt);
                ast_received.setOfficeLocation(ofcLocation);
                ast_received.setCondition(conditionResult);

                assert srvc_asset_received_table_key != null;
                srvc_ast_received.child(srvc_asset_received_table_key).setValue(ast_received);
                Firebase.setAndroidContext(getApplicationContext());
                finish();
                startActivity(new Intent(ServiceAssetReceived.this, LastPage.class));

                /**
                 * Showing Toast message after successfully data submit.
                 */
                Toast.makeText(ServiceAssetReceived.this,"Record updated successfully", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void sendEmail() {

        String scan_txt = scanText.getText().toString();

        String email = "rkhariharan1997@gmail.com";   //Mail address of recipient
        String subject = "Asset Scrap Notifier";
        String message = ("Asset ID: "+ scan_txt +" has been scrapped");

        Log.d("result",""+email);
        Log.d("result1",""+subject);
        Log.d("result2",""+message);
        /**
         * Creating SendMail object
         */
        SendMail sm = new SendMail(this, email, subject, message);

        /**
         * Executing sendmail to send email
         */
        sm.execute();
    }

    /**
     * Getting the QR Scan results
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data
                try {
                    //converting the data to json
                    JSONObject obj = new JSONObject(result.getContents());
                    //setting values to textviews

                } catch (JSONException e) {
                    e.printStackTrace();
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                    scanText.setText(result.getContents());
                    scanText.setVisibility(View.VISIBLE);
                    tbl_lyt.setVisibility(View.VISIBLE);
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * Validate Function
     */
    private boolean validateForm() {

        boolean result = validateTextView(R.id.scanText, R.string.err_msg_required);
        return result;
    }

    private boolean validateTextView(int id,int error_message_res_id) {

        /**
         * Obtain the Edit widget
         */
        TextView txt =(TextView)findViewById(id);

        /**
         * Validate button not pressed
         */
        if(txt.getText().toString().trim().isEmpty()){
            scanText.setVisibility(View.VISIBLE);
            txt.setError(getString(error_message_res_id));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setMessage("Are you sure to cancel?")
            .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                // do something when the button is clicked
                public void onClick(DialogInterface arg0,int arg1) {
                    ServiceAssetReceived.super.onBackPressed();
                    finish();
                    //close();
                }
            })
            .setNegativeButton("No",new DialogInterface.OnClickListener() {

                // do something when the button is clicked
                public void onClick(DialogInterface arg0,int arg1) {
                }
            })
            .show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView,View view,int pos,long l) {
        if (pos==4) {
            // Set an EditText view to get user input
            final EditText input = new EditText(this);

            new AlertDialog.Builder(this)
                .setMessage("Enter your site location")
                .setView(input)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    public void onClick(DialogInterface dialog,int whichButton) {
                        Editable editable = input.getText();
                        String newString = editable.toString();

                        ofc_loc.clear();
                        ofc_loc.add(newString);
                        adapter.notifyDataSetChanged();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Do nothing.
                        spinner.setAdapter(adapter);

                    }
                }).show();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}
