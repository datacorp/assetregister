package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * ContactActivity used to get help via mail,website,youtube links
 */
public class ContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.activity_contact_us);

        /**
         * Adding an OnClickListener to mail
         */
        ((Button)findViewById(R.id.mail)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gotoMail();
            }
        });

        /**
         * Adding an OnClickListener to website
         */
        ((Button)findViewById(R.id.website)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gotoWebsite();
            }
        });

        /**
         * Adding an OnClickListener to youtube
         */
        ((Button)findViewById(R.id.youtube)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoYoutube();
            }
        });
    }

    /**
     * Function to create mail
     */
    protected void gotoMail() {
        String[] TO = {"hari97.rk@gmail.com"};
        String[] CC = {""};
        String subject= "Customer Feedback";
        Intent mailIntent = new Intent(Intent.ACTION_SEND);

        mailIntent.setData(Uri.parse("mailto:"));
        mailIntent.setType("text/plain");
        mailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        mailIntent.putExtra(Intent.EXTRA_CC, CC);
        mailIntent.putExtra(Intent.EXTRA_SUBJECT,subject);
//        mailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

        try {
            startActivity(Intent.createChooser(mailIntent, "Sending..."));
            finish();
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ContactActivity.this, "Mail client not installed.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Function to go datacorp website
     */
    public void  gotoWebsite(){
        Intent b = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.datacorp-traffic.in/"));
        startActivity(b);
    }

    /**
     * Function to go youtube page
     */
    public void  gotoYoutube(){
        Intent b = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/watch?v=r8tjugZBN-Q"));
        startActivity(b);
    }

}
