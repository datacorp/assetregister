package in.datacorp.traffic.dcsitemateriallist.model;

public class User {

    /**
     * User data structures
     */
    private String userID;
    private String userName;
    private String userEmail;
    private String userRole;

    public User(){

    }

    public User(String uID,String uName,String uEmail,String uRole) {
        this.userID = uID;
        this.userName = uName;
        this.userEmail = uEmail;
        this.userRole = uRole;
    }

    /**
     * Getter for UserID
     */
    public String getUserId() {
        return userID;
    }

    /**
     * Getter for UserName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Getter for UserEmailID
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * Getter for UserRole
     */
    public String getUserRole() {
        return userRole;
    }

}
