package in.datacorp.traffic.dcsitemateriallist;

/**
 * import statement
 */
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.datacorp.traffic.dcsitemateriallist.model.User;

/**
 * A SignUp screen that offers users to signup for the app with User Name, Admin Registered Email and a Password.
 */
public class SignUpActivity extends AppCompatActivity {

    /**
     * Keep track of the User login.
     */
    private FirebaseAuth auth;
    /**
     * String of database reference to check whether user is created by admin person already
     */
    DatabaseReference user_db_ref;
    /**
     * EditText field for User Name
     */
    private EditText editText_userName;
    /**
     * EditText field for User Email
     */
    private EditText editText_Email;
    /**
     * EditText field for Password
     */
    private EditText editText_password;
    /**
     * EditText field for confirm password
     */
    private EditText editText_confirmPassword;
    /**
     * Button to SignUp
     */
    private Button btn_create_user;
    /**
     * TAG value to show in log message on successful user registration
     */
    private String TAG = "New User Registration";
    /**
     * String value that store UserName value entered in User Name EditText field
     */
    protected String UserName;
    /**
     * String value that store User Email value entered in User Email EditText field
     */
    protected String Email;
    /**
     * String value that store password value entered in password EditText field
     */
    protected String Password;
    /**
     * String value that store password value entered in confirm password EditText field
     */
    protected String ConfirmPassword;
    /**
     * Pattern Variable to validate the email address
     */
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.activity_signup);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        user_db_ref = FirebaseDatabase.getInstance().getReference("User");

        auth = FirebaseAuth.getInstance();

        /**
         * Creating User Interface
         */
        editText_Email = (EditText) findViewById(R.id.EditText_RegisteredEmail);

        editText_password = (EditText) findViewById(R.id.EditText_password);

        editText_confirmPassword = (EditText) findViewById(R.id.Edittext_confirmPassword);

        btn_create_user = (Button) findViewById(R.id.button_sign_up);

        Email = editText_Email.getText().toString();

        Password = editText_password.getText().toString();

        ConfirmPassword = editText_confirmPassword.getText().toString();

        /**
         * Button to create new user
         */
        btn_create_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**
                 * This ensures all the fields are not empty and starts a signup process.
                 */
                if(!(TextUtils.isEmpty(editText_Email.getText())) && !(TextUtils.isEmpty(editText_password.getText())) && !(TextUtils.isEmpty(editText_confirmPassword.getText())) &&
                        Password.contentEquals(ConfirmPassword) && isEmailValid(editText_Email.getText().toString()) && isPasswordValid(editText_password.getText().toString())
                        && isPasswordValid(editText_confirmPassword.getText().toString())){

                    /**
                     * Checking entered email already created by admin already using user_db_ref, else new user not created
                     */
                    user_db_ref.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                                final User user = postSnapshot.getValue(User.class);

                                /**
                                 * Check the user provided email with the users emails in Users tree in firebase
                                 * If there is an email present in the users tree then we can invoke the createUserWithEmailAndPassword function.
                                 */
                                if(user.getUserEmail().contentEquals(editText_Email.getText().toString())){
                                    auth.createUserWithEmailAndPassword(editText_Email.getText().toString(),
                                            editText_password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (!task.isSuccessful()) {
                                                Log.d(TAG,"Authentication failed." + task.getException());

                                            } else {
                                                final String role = user.getUserRole();
                                                Log.d("Value",""+role);
                                                if(role.equals("User")){
                                                    Toast.makeText(getApplicationContext(), "Your Role "+user.getUserRole(), Toast.LENGTH_SHORT).show();
                                                    startActivity(new Intent(SignUpActivity.this, IndexActivity.class));
                                                    finish();

                                                }
                                                else if(role.equals("Admin")){
                                                    Toast.makeText(getApplicationContext(), "Your Role "+user.getUserRole(), Toast.LENGTH_SHORT).show();
                                                    startActivity(new Intent(SignUpActivity.this, AdminIndexActivity.class));
                                                    finish();
                                                }
                                            }
                                        }
                                    });

                                }
                                /**
                                 * If user provided email is not in users tree then it will show role toast error message.
                                 */
                                else{
//                                    Toast.makeText(SignUpActivity.this, "this mail ID not created by admin ",Toast.LENGTH_SHORT).show();
                                    Toast.makeText(SignUpActivity.this, "Fail to load ",Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }); }

                /**
                 * If Email address field is empty it shows an error at Email address field.
                 */
                else if(TextUtils.isEmpty(editText_Email.getText()) || !isEmailValid(editText_Email.getText().toString())){
                    editText_Email.requestFocus();
                    if(TextUtils.isEmpty(editText_Email.getText())){
                        editText_Email.setError("Please Enter the Email Address");
                    }else{
                        editText_Email.setError("Invalid Email");
                    }
                }

                /**
                 * If password field is empty it shows an error at password field.
                 */
                else if(TextUtils.isEmpty(editText_password.getText()) || !isPasswordValid(editText_password.getText().toString())){
                    editText_password.requestFocus();
                    if(TextUtils.isEmpty(editText_password.getText())){
                        editText_password.setError("Please Enter the Password");
                    } else {
                        editText_password.setError("Password must be of minimum 6 characters");
                    }
                }
                /**
                 * If confirm password field is empty it shows an error at confirm password field.
                 */
                else if(TextUtils.isEmpty(editText_confirmPassword.getText()) || !isPasswordValid(editText_confirmPassword.getText().toString())){
                    editText_confirmPassword.requestFocus();
                    if(TextUtils.isEmpty(editText_confirmPassword.getText())){
                        editText_confirmPassword.setError("Please re-enter the new password");
                    } else {
                        editText_confirmPassword.setError("Password must be of minimum 6 characters");
                    }
                }
                /**
                 * If password mismatches it shows an error at password field.
                 */
                else if (!(Password.contentEquals(ConfirmPassword))){
                    editText_confirmPassword.requestFocus();
                    editText_confirmPassword.setError("Password mismatch");
                }
            }
        });
    }

    /**
     * This function checks the email validity
     * @param email
     * @return True if Email is valid else false
     */
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
        return matcher.find();
    }

    /**
     * This function checks for password validity
     * @param password
     * @return Ture if password is valid else false
     */
    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 6;
    }

    /**
     * This function invokes the normal Back_key press event and shows a confirmation dialog that asks to close the current activity.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure to cancel the new user?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0,int arg1) {
                        SignUpActivity.super.onBackPressed();
                        finish();
                        //close();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0,int arg1) {
                    }
                })
                .show();
    }
    /**
     * This function is used to invoke the Back button in navigation bar.
     * @return Boolean true
     */
    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }
}
