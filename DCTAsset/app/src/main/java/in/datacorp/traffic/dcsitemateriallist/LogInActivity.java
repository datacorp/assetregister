package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import in.datacorp.traffic.dcsitemateriallist.model.User;

/**
 * LogInActivity used checks authentication of users
 */
public class LogInActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    /**
     * String of database path for registered users
     */
    public static final String user_db_path = "User";

    /**
     * FirebaseAuth for user sync
     */
    private FirebaseAuth auth;

    /**
     * Database reference to check whether user registered by admin
     */
    DatabaseReference db_reference;

    /**
     * ProgressBar used to load, util syncing of data
     */
    private ProgressBar progressBar;

    /**
     * EditText for email, password
     */
    private EditText ed_email, ed_password;

    private TextInputLayout lyt_ed_email, lyt_ed_password;

    int invalidEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.activity_login);

        /**
         * Getting the reference of user from user tree
         */
        db_reference = FirebaseDatabase.getInstance().getReference(user_db_path);

        /**
         * Firebase authentication
         */
        auth = FirebaseAuth.getInstance();

        /**
         * Creating UI reference
         */
        ed_email = (EditText) findViewById(R.id.login_input_email);

        lyt_ed_email = (TextInputLayout) findViewById(R.id.login_input_layout_email);

        ed_password = (EditText) findViewById(R.id.login_input_password);

        lyt_ed_password = (TextInputLayout) findViewById(R.id.login_input_layout_password);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        TextView txtv_forgot_password = (TextView) findViewById(R.id.textView_forgotPassword);

        Button btn_login = (Button) findViewById(R.id.btn_login);

        Button btn_signUp = (Button) findViewById(R.id.btn_link_signup);

        /**
         * Adding an onclicklistener to btn_login
         */
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        /**
         * Adding an onclicklistener to txtv_forgot_password
         */
        txtv_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LogInActivity.this,ForgotPasswordActivity.class));
            }
        });

        /**
         * Adding an onclicklistener to btn_signUp
         */
        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LogInActivity.this,SignUpActivity.class);
                startActivity(intent);
            }
        });

    }

    private void submitForm() {
        final String email = ed_email.getText().toString().trim();
        final String password = ed_password.getText().toString().trim();

        /**
         * Validating form
         */
        if(!checkEmail()) {
            return;
        }
        if(!checkPassword()) {
            return;
        }

        lyt_ed_email.setErrorEnabled(false);

        lyt_ed_password.setErrorEnabled(false);

        progressBar.setVisibility(View.VISIBLE);

        /**
         * Root admin login
         */
//        if(email.equals("admin@dc.in") && password.equals("dcadmin")) {
//
//            Intent intent = new Intent(LogInActivity.this, AdminPage.class);
//            startActivity(intent);
//            finish();
//        }

        if (email.contentEquals("siteadmin@gmail.com") ||email.contentEquals("admin@gmail.com")){
            /**
             * authenticate user
             */
            auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(LogInActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            /**
                             * If sign in fails, display a message to the user. If sign in succeeds
                             * the auth state listener will be notified and logic to handle the
                             * signed in user can be handled in the listener.
                             */
                            progressBar.setVisibility(View.GONE);
                            if (!(task.isSuccessful())) {
                                /**
                                 * there was an error
                                 */
                                Toast.makeText(LogInActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();

                            } else {
                                Intent intent = new Intent(LogInActivity.this, AdminIndexActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
        }

        /**
         * User login,checking entered ed_email is matched with user tree
         */
        db_reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    invalidEmail =0;
                    User user = postSnapshot.getValue(User.class);

                    /**
                     * Check the user provided email with the users emails in Users tree in firebase
                     * If there is an email present in the users tree then we can invoke the createUserWithEmailAndPassword function.
                     */
                    assert user != null;
                    if(user.getUserEmail().contentEquals(email)){

                        String role = user.getUserRole();
                        Log.d("Value",""+role);

                        if(role.equals("User")){                            //authenticate user
                            auth.signInWithEmailAndPassword(email, password)
                                    .addOnCompleteListener(LogInActivity.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {

                                            progressBar.setVisibility(View.GONE);
                                            if (!task.isSuccessful()) {
                                                // there was an error
                                                Toast.makeText(LogInActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();

                                            } else {
                                                Intent intent = new Intent(LogInActivity.this, IndexActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                    });
                            Toast.makeText(getApplicationContext(), "Your Role "+user.getUserRole(), Toast.LENGTH_SHORT).show();

                        }

                        else if(role.equals("Admin")){
                            auth.signInWithEmailAndPassword(email, password)
                                    .addOnCompleteListener(LogInActivity.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {

                                            progressBar.setVisibility(View.GONE);
                                            if (!task.isSuccessful()) {
                                                // there was an error
                                                Toast.makeText(LogInActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();

                                            } else {
                                                Intent intent = new Intent(LogInActivity.this, AdminIndexActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                    });
                            Toast.makeText(getApplicationContext(), "Your Role "+user.getUserRole(), Toast.LENGTH_SHORT).show();

                        }

                        else if(role.equals("Manager")){
                            auth.signInWithEmailAndPassword(email, password)
                                    .addOnCompleteListener(LogInActivity.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {

                                            progressBar.setVisibility(View.GONE);
                                            if (!task.isSuccessful()) {
                                                // there was an error
                                                Toast.makeText(LogInActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();

                                            } else {
                                                Intent intent = new Intent(LogInActivity.this, ManagerActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                    });

                            Toast.makeText(getApplicationContext(), "Your Role "+user.getUserRole(), Toast.LENGTH_SHORT).show();
                        }

                        /**
                         * If user provided email is not in users tree then it will show a error message.
                         */
                        else{
                            Toast.makeText(getApplicationContext(), "this mail ID not created by admin", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        invalidEmail = 1;
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        if(invalidEmail ==1){
            Toast.makeText(LogInActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Function to validate email
     * @return
     */
    private boolean checkEmail() {
        String email = ed_email.getText().toString().trim();
        if (email.isEmpty() || !isEmailValid(email)) {

            lyt_ed_email.setErrorEnabled(true);
            lyt_ed_email.setError(getString(R.string.err_msg_email));
            ed_email.setError(getString(R.string.err_msg_required));
            requestFocus(ed_email);
            return false;
        }
        lyt_ed_email.setErrorEnabled(false);
        return true;
    }

    /**
     * Function to validate password
     * @return
     */
    private boolean checkPassword() {

        String password = ed_password.getText().toString().trim();
        if (password.isEmpty() || !isPasswordValid(password)) {

            lyt_ed_password.setError(getString(R.string.err_msg_password));
            ed_password.setError(getString(R.string.err_msg_required));
            requestFocus(ed_password);
            return false;
        }
        lyt_ed_password.setErrorEnabled(false);
        return true;
    }

    private static boolean isEmailValid(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private static boolean isPasswordValid(String password){
        return (password.length() >= 6);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }

}
