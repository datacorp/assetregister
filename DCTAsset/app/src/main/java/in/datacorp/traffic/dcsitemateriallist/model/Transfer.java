package in.datacorp.traffic.dcsitemateriallist.model;

/**
 * Import library
 */
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * This is a model class for transfer asset
 */
@IgnoreExtraProperties
public class Transfer {

    /**
     * Asset Install date
     */
    private String creation_date;

    /**
     * Asset Transfer data structure
     */
    private String nxt_owner_name;
    private String office_location;
    private String condition;
    private Boolean is_received;
    private String location;

    /**
     * Class Constructor
     */
    public Transfer() {

    }

    /**
     * Setter for Transfer Date Time
     */
    public void setCurrentDateTime(String date) {
        this.creation_date = date;
    }
    /**
     * Getter for Transfer Date & Time
     */
    public String getcurrentDateTime() {
        return creation_date;
    }

    /**
     * Setter for New Asset Owner Name
     */
    public void setAssetNextOwner(String owner_name) {
        this.nxt_owner_name = owner_name;
    }
    /**
     * Getter for New Asset Owner Name
     */
    public String getassetNextOwner() {
        return nxt_owner_name;
    }

    /**
     * Setter for New Office Location(the place asset was created)
     */
    public void setOfficeLocation(String ofc_location) {
        this.office_location = ofc_location;
    }
    /**
     * Getter for New Office Location(the place asset was created)
     */
    public String getofficeLocation() {
        return office_location;
    }

    /**
     * Setter for Asset Condition
     */
    public void setCondition(String conditionResult) {
        this.condition = conditionResult;
    }
    /**
     * Getter for Asset Condition
     */
    public String getcondition() {
        return condition;
    }

    /**
     * Setter for Asset Received/Not Received
     */
    public void setReceived(Boolean rec) {
        this.is_received = rec;
    }
    /**
     * Getter for Asset Received/Not Received
     */
    public Boolean getreceived(){
        return is_received;
    }

    /**
     * Setter for Asset Current Location (latitude and longitude)
     */
    public void setCurrentLocation(String loc) {
        location = loc;
    }
    /**
     * Getter for Asset Current Location (latitude and longitude)
     */
    public String getcurrentLocation() {
        return location;
    }

}
