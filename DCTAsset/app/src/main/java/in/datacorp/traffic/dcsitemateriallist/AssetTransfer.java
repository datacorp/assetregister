package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.datacorp.traffic.dcsitemateriallist.model.Register;
import in.datacorp.traffic.dcsitemateriallist.model.Transfer;

/**
 * This class used to store the asset transfer details
 */
public class AssetTransfer extends AppCompatActivity implements AdapterView.OnItemSelectedListener, LocationListener{

    /**
     * List to get all the item from database
     */
    List<Transfer> item;

    /**
     * IntentIntegrator for qr scanner
     */
    private IntentIntegrator qrScan;

    /**
     * TextView to store qr scanned text
     */
    private TextView scanText;

    /**
     * Creating Unique table id
     */
    private String id;

    /**
     * Spinner for office location
     */
    private Spinner spinner;

    /**
     * RadioButton to get working condition of asset
     */
    private RadioGroup conditionGroup;

    private RadioButton conditionButton;

    /**
     * TableLayout display after getting scan text result
     */
    private TableLayout tbl_lyt;

    /**
     * Location Manager to get location
     */
    LocationManager locationManager;

    /**
     * String to store location, address
     */
    String  locationHolder ,addressHolder;

    /**
     * TextView to display location text
     */
    TextView locationTxt;

    /**
     * ArrayAdapter for adapter view
     */
    ArrayAdapter adapter;

    /**
     * ArrayList to store office location
     */
    ArrayList<String> ofc_loc;

    private Boolean flash = false;

    /**
     * String of database reference to check whether asset registered before transferring
     */
    public static final String Database_Path = "Asset_Install";

    /**
     * Database reference to get asset records
     */
    private DatabaseReference asset_transfer_db_ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.asset_transfer);

        /**
         * Getting the reference of asset transfer node
         */
        asset_transfer_db_ref = FirebaseDatabase.getInstance().getReference(Database_Path);

        /**
         * Creating UI reference
         */
        tbl_lyt = (TableLayout)findViewById(R.id.table2);

        conditionGroup = (RadioGroup) findViewById(R.id.asset_condition);

        qrScan = new IntentIntegrator(this);

        scanText = (TextView)findViewById(R.id.scanText);

        spinner = (Spinner) findViewById(R.id.spinnerGenres);

        locationTxt = (TextView)findViewById(R.id.txt_location);

        tbl_lyt.setVisibility(View.INVISIBLE);

        scanText.setVisibility(View.INVISIBLE);

        locationTxt.setVisibility(View.INVISIBLE);

        /**
         * Create an ArrayAdapter using the string array and a default spinner layout
         */
        ofc_loc = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.office_location)));

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,ofc_loc);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // Specify the layout to use when the list of choices appears

        spinner.setAdapter(adapter); // Display the adapter in spinner list

        spinner.setOnItemSelectedListener(this);

        /**
         * Arraylist to store item records
         */
        item = new ArrayList<>();

        /**
         * Adding an OnClickListener to buttonScan
         */
        ((Button) findViewById(R.id.buttonScan)).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                qrScan.initiateScan();
            }
        });

        /**
         * Adding an OnClickListener to btn_location
         */
        ((Button)findViewById(R.id.btn_location)).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getLocation();
            }
        });

        /**
         * Adding an OnClickListener to buttonScan
         */
        ((Button) findViewById(R.id.saveItems)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                asset_transfer_db_ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                            String scanAddress = scanText.getText().toString();

                            /**
                             * Getting data model
                             */
                            Register register = postSnapshot.getValue(Register.class);
                            assert register != null;

                            Log.d("SavedQR ","" + register.getscanSerialNumber());
                            if (register.getscanSerialNumber().contentEquals(scanAddress)) {

                                id = register.getTableID();
                                flash = true;
                                Log.d("Result",""+ true);
                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                if(flash){

                    /**
                     * Validate Form
                     */
                    if (!validateForm()) {
                        return;
                    }
                    Log.d("flashID",""+id);
                    Log.d("flash",""+flash);

                    int conditionID = conditionGroup.getCheckedRadioButtonId();
                    conditionButton = (RadioButton) findViewById(conditionID);
                    String conditionResult = conditionButton.getText().toString();

                    String ofcLocation = spinner.getSelectedItem().toString();

                    CheckBox chk = (CheckBox) findViewById(R.id.received);
                    boolean rec = chk.isChecked();

                    /**
                     * Set Current Date & Time as the site install time
                     */
                    @SuppressLint("SimpleDateFormat")
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
                    final String transfer_date = sdf.format(new Date());

                    /**
                     * Creating data model for Transfer
                     */
                    Transfer trans = new Transfer();

                    trans.setCurrentDateTime(transfer_date);
                    trans.setAssetNextOwner(getEditTextData(R.id.txt_asset_owner));
                    trans.setOfficeLocation(ofcLocation);
                    trans.setCondition(conditionResult);
                    trans.setReceived(rec);
                    trans.setCurrentLocation(locationHolder);

                    asset_transfer_db_ref.child(id).child("asset_transfer_records").push().setValue(trans);
                    Firebase.setAndroidContext(getApplicationContext());
                    finish();
                    startActivity(new Intent(AssetTransfer.this, LastPage.class));

                    /**
                     * Showing Toast message after successfully data submit.
                     */
                    Toast.makeText(AssetTransfer.this,"Record updated successfully", Toast.LENGTH_LONG).show();

                }
                else{

                    Toast.makeText(AssetTransfer.this,"ERROR: Scanned asset not registered", Toast.LENGTH_LONG).show();
                    Log.d("flash else",""+flash);
                }
            }
        });
    }

    /**
     * Getting the QR Scan results
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data
                try {
                    //converting the data to json
                    JSONObject obj = new JSONObject(result.getContents());
                    //setting values to textviews

                } catch (JSONException e) {
                    e.printStackTrace();
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                    scanText.setText(result.getContents());
                    scanText.setVisibility(View.VISIBLE);
                    tbl_lyt.setVisibility(View.VISIBLE);
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * set the current values of an Location & Address
     */
    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            assert locationManager != null;
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,5000,5,this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,5000,5,(LocationListener) this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n") //Indicates that line should ignore the specified warnings for the annotated element whenever something in our code isn't optimal or may crash
    public void onLocationChanged(Location location) {

        locationTxt.setVisibility(View.VISIBLE);

        locationHolder = "Latitude: " + location.getLatitude() + ", Longitude: " + location.getLongitude(); //To save location (lat/lon) in FD
        locationTxt.setText("Latitude: " + location.getLatitude() + " Longitude: " + location.getLongitude()); //Display the location

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            locationTxt.setText(locationTxt.getText() + "\n"
                    +addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "
                    +addresses.get(0).getAddressLine(2)); //Display the address
            addressHolder = addresses.get(0).getAddressLine(0); //To save address in FD

        }
        catch(Exception ignored)
        {

        }
    }

    public void onProviderDisabled(String provider) {
        Toast.makeText(AssetTransfer.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();

    }

    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    public void onProviderEnabled(String provider) {

    }

    private String getEditTextData(int id) {

        /**
         * Obtain the Edit widget
         */
        EditText et = (EditText)findViewById(id);
        return et.getText().toString().trim();
    }

    /**
     * Validate Function
     */
    private boolean validateForm() {

        boolean result = validateTextView(R.id.scanText, R.string.err_msg_required);
        result &= validateEditText(R.id.txt_asset_owner, R.id.txt_lyt_asset_owner, R.string.err_msg_required);
        result &= validateTextView(R.id.txt_location, R.string.get_location);

        return result;
    }

    private boolean validateEditText(int id, int text_layout_id, int error_message_res_id) {

        /**
         * Obtain the Edit widget
         */
        EditText et = (EditText)findViewById(id);
        TextInputLayout txtlyt = (TextInputLayout)findViewById(text_layout_id);

        /**
         * Validate if it is empty
         */
        if (et.getText().toString().trim().isEmpty()) {
            et.setError(getString(error_message_res_id));
            txtlyt.setErrorEnabled(true);
            return false;
        }
        /**
         * Not Empty, Clear error and return success
         */
        txtlyt.setErrorEnabled(false);
        return true;
    }

    private boolean validateTextView(int id,int error_message_res_id) {

        /**
         * Obtain the Edit widget
         */
        TextView txt =(TextView)findViewById(id);
        /**
         * Validate button not pressed
         */
        if(txt.getText().toString().trim().isEmpty()){
            scanText.setVisibility(View.VISIBLE);
            locationTxt.setVisibility(View.VISIBLE);
            txt.setError(getString(error_message_res_id));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setMessage("Are you sure to cancel?")
            .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                // do something when the button is clicked
                public void onClick(DialogInterface arg0,int arg1) {
                    AssetTransfer.super.onBackPressed();
                    finish();
                    //close();
                }
            })
            .setNegativeButton("No",new DialogInterface.OnClickListener() {

                // do something when the button is clicked
                public void onClick(DialogInterface arg0,int arg1) {
                }
            })
            .show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView,View view,int pos,long l) {
        if (pos==4) {
            /**
             * Set an EditText view to get user input
             */
            final EditText input = new EditText(this);

            new AlertDialog.Builder(this)
                .setMessage("Enter your site location")
                .setView(input)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    public void onClick(DialogInterface dialog,int whichButton) {
                        Editable editable = input.getText();
                        String newString = editable.toString();

                        ofc_loc.clear();
                        ofc_loc.add(newString);
                        adapter.notifyDataSetChanged();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Do nothing.
                    }
                }).show();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
