package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;

/**
 * A Change Password screen that offers users to change the their current login password.
 */
public class NewPasswordActivity extends AppCompatActivity {

    /**
     * EditText field for Current Password
     */
    private EditText edtCurrentPassword;
    /**
     * EditText field for New Password
     */
    private EditText edtNewPassword;
    /**
     * EditText field for New Password
     */
    private EditText edtConfirmNewPassword;
    /**
     * Firebase Authentication object
     */
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.activity_new_password);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /**
         * Creating UI reference
         */
        edtCurrentPassword = (EditText) findViewById(R.id.et_currentPassword);

        edtNewPassword = (EditText) findViewById(R.id.et_newPassword);

        edtConfirmNewPassword = (EditText) findViewById(R.id.et_confirmNewPassword);

        Button btnChangePassword = (Button) findViewById(R.id.btn_setPassword);

        /**
         * Adding an onclicklistener to btnChangePassword
         */
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String currentPassword = edtCurrentPassword.getText().toString().trim();

                final String newPassword = edtNewPassword.getText().toString().trim();

                final String confirmNewPassword = edtConfirmNewPassword.getText().toString().trim();

                mAuth = FirebaseAuth.getInstance();

                /**
                 * Checking for current password
                 */
                if (!(TextUtils.isEmpty(currentPassword)) && isPasswordValid(currentPassword)) {

                    /**
                     * If current password is not empty then we re-authenticate the user with his/her email id and current login password to change their password.
                     */
                    mAuth.getCurrentUser().reauthenticate(EmailAuthProvider.getCredential(mAuth.getCurrentUser().getEmail(),currentPassword)).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if(task.isSuccessful()){

                                if(TextUtils.isEmpty(newPassword) || !isPasswordValid(newPassword)){
//                                    Toast.makeText(getApplicationContext(), "Enter new password", Toast.LENGTH_SHORT).show();
                                    edtNewPassword.requestFocus();
                                    edtNewPassword.setError("Password should not be empty or must be of minimum 6 characters");
                                    return;
                                }
                                else if(TextUtils.isEmpty(confirmNewPassword) || !isPasswordValid(confirmNewPassword)){
//                                    Toast.makeText(getApplicationContext(), "Please enter the new password", Toast.LENGTH_SHORT).show();
                                    edtConfirmNewPassword.requestFocus();
                                    edtConfirmNewPassword.setError("Password should not be empty or must be of minimum 6 characters");
                                    return;
                                }
                                else{
                                     if (newPassword.equals(confirmNewPassword)) {
                                        mAuth.getCurrentUser().updatePassword(confirmNewPassword).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Toast.makeText(NewPasswordActivity.this, "Password changed successfully", Toast.LENGTH_SHORT).show();

                                                    /**
                                                     * This shows a success dialog on successful password change.
                                                     */
                                                    new AlertDialog.Builder(NewPasswordActivity.this)
                                                            .setMessage("Password changed successfully")
                                                            .setCancelable(false)
                                                            .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                                                                // do something when the button is clicked
                                                                public void onClick(DialogInterface arg0,int arg1) {
                                                                    NewPasswordActivity.super.onBackPressed();
                                                                    startActivity(new Intent(NewPasswordActivity.this, IndexActivity.class));
                                                                    finish();
                                                                    //close();
                                                                }
                                                            })
                                                            .show();
                                                } else {
                                                    Toast.makeText(NewPasswordActivity.this, "Failed to change password! Try again.", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    }
                                    else{
                                        edtConfirmNewPassword.requestFocus();
                                        edtConfirmNewPassword.setError("Password mismatch");
                                        return;
                                    }
                                }
                            }
                            else{
                                Toast.makeText(getApplicationContext(), "You entered an incorrect password", Toast.LENGTH_SHORT).show();
                                edtCurrentPassword.requestFocus();
                                edtCurrentPassword.setError("Incorrect password");
                                return;
                            }
                        }
                    });

                }
                else{
                    if((TextUtils.isEmpty(currentPassword))){
                        edtCurrentPassword.requestFocus();
                        edtCurrentPassword.setError("Password should not be empty");
                        return;
                    }else {
                        edtCurrentPassword.requestFocus();
                        edtCurrentPassword.setError("Password must be of minimum 6 characters");
                    }

                }
            }
        });

    }

    /**
     * Function to validate password
     * @param password
     * @return Ture if password is valid else false
     */
    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 6;
    }

    /**
     * Function to invoke the Back button in navigation bar.
     * @return true
     */
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    /**
     * Function to invokes the normal Back_key press event and closes the current activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
