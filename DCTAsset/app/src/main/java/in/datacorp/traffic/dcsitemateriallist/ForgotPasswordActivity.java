package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

/**
 * ForgotPasswordActivity, enter your mail to reset your password.
 */
public class ForgotPasswordActivity extends AppCompatActivity {

    /**
     * EditeText to get password
     */
    EditText edt_password_reset_email;

    /**
     * Button to reset password
     */
    Button btn_reset_password;

    /**
     * FirebaseAuth to get current mail id
     */
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.activity_forgot_password);

        /**
         * Firebase authentication
         */
        auth = FirebaseAuth.getInstance();

        /**
         * Creating UI reference
         */
        edt_password_reset_email = (EditText)findViewById(R.id.editTextPasswordResetEmail);

        btn_reset_password = (Button)findViewById(R.id.button_sendEmailtoResetPassword);

        /**
         * Adding an onclicklistener to btn_reset_password
         */
        btn_reset_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**
                 * Creating local variables to save details
                 */
                String email = edt_password_reset_email.getText().toString().trim();

                /**
                 * If editText is empty, shows a message asking to enter an email.
                 */
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter your email!", Toast.LENGTH_SHORT).show();
                    return;
                }

                /**
                 * Sends a link to the email. To reset the password, click on the link.
                 */
                auth.sendPasswordResetEmail(email)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(ForgotPasswordActivity.this, "Check email to reset your password!", Toast.LENGTH_SHORT).show();

                                    new AlertDialog.Builder(ForgotPasswordActivity.this)
                                            .setMessage("Check email to reset your password!")
                                            .setCancelable(false)
                                            .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                                                /**
                                                 *  do something when the button is clicked
                                                 * @param arg0
                                                 * @param arg1
                                                 */
                                                public void onClick(DialogInterface arg0,int arg1) {
                                                    ForgotPasswordActivity.super.onBackPressed();
                                                    startActivity(new Intent(ForgotPasswordActivity.this,LogInActivity.class));
                                                    finish();
                                                    /**
                                                     * close
                                                     */
                                                }
                                            })
                                            .show();
                                } else {
                                    Toast.makeText(ForgotPasswordActivity.this, "Fail to send reset password email!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setMessage("Are you sure to cancel?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    /**
                     *  do something when the button is clicked
                     * @param arg0
                     * @param arg1
                     */
                    public void onClick(DialogInterface arg0,int arg1) {
                        ForgotPasswordActivity.super.onBackPressed();
                        startActivity(new Intent(ForgotPasswordActivity.this,LogInActivity.class));
                        finish();
                        /**
                         * close();
                         */
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    /**
                     * do something when the button is clicked
                     * @param arg0
                     * @param arg1
                     */
                    public void onClick(DialogInterface arg0,int arg1) {
                    }
                })
                .show();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
