package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.datacorp.traffic.dcsitemateriallist.model.User;

public class AdminAddUserActivity extends AppCompatActivity {

    /**
     * EditText for name
     */
    EditText editTextName;

    /**
     * EditText for email
     */
    EditText editTextEmail;

    /**
     * Spinner to drop down the list
     */
    Spinner spinner;

    /*
     * Button to save user
     */
    Button buttonAdd;

    /**
     * List to get all the user from database
     */
    List<User> user;

    /**
     * Email validation
     */
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    /**
     * Database reference to store new users
     */
    DatabaseReference userDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_add_user);

        /**
         * Getting the reference of user node
         */
        userDatabaseReference = FirebaseDatabase.getInstance().getReference("User");

        /**
         * Creating UI reference
         */
        editTextName = (EditText) findViewById(R.id.editTextName);

        editTextEmail = (EditText) findViewById(R.id.editTextEmail);

        spinner = (Spinner) findViewById(R.id.spinnerGenres);

        buttonAdd = (Button) findViewById(R.id.userAdd);

        /**
         * Arraylist to store user
         */
        user = new ArrayList<>();

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /**
                 * Validation Form
                 */
                if (!validateForm()) {
                    return;
                }else {
                    addUser();
                }
            }
        });
    }

    /**
     * Function to add user in database
     */
    private void addUser() {

        /**
         * Creating local variables to save details
         */
        String name = editTextName.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
        String role = spinner.getSelectedItem().toString();

        if (isEmailValid(editTextEmail.getText().toString())) {

            /**
             * Creating Unique table ID
             */
            String id = userDatabaseReference.push().getKey();

            /**
             * User data model reference
             */
            User user = new User(id, name, email,role);

            userDatabaseReference.child(id).setValue(user);

            editTextName.setText("");

            editTextEmail.setText("");

            //displaying a success toast
            Toast.makeText(this, "User added successfully ", Toast.LENGTH_LONG).show();
        } else {
            //if the value is not given displaying a toast
            Toast.makeText(this, "Please enter valid mailID ", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Function to validate email pattern
     * @param email
     * @return
     */
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
        return matcher.find();
    }

    /**
     * Validate form to check whether the data is entered
     * @return
     */
    private boolean validateForm() {

        boolean result = validateEditText(R.id.editTextName, R.id.txt_lyt_user_name, R.string.err_msg_required);
        result &= validateEditText(R.id.editTextEmail, R.id.txt_lyt_user_email, R.string.err_msg_required);
        return result;
    }

    private boolean validateEditText(int id, int text_layout_id, int error_message_res_id) {

        /**
         * Obtain the Edit widget
         */
        EditText et = (EditText)findViewById(id);
        TextInputLayout txtlyt = (TextInputLayout)findViewById(text_layout_id);

        /**
         * Validate if it is empty
         */
        if (et.getText().toString().trim().isEmpty()) {
            et.setError(getString(error_message_res_id));
            txtlyt.setErrorEnabled(true);
            return false;
        }

        /**
         * Not Empty, Clear error and return success
         */
        txtlyt.setErrorEnabled(false);
        return true;
    }

}
