package in.datacorp.traffic.dcsitemateriallist.model;

/**
 * Import library
 */
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * This is a model class for registering asset service details
 */
public class Service {

    /**
     * Asset Creation/Install date
     */
    private String creation_date;

    /**
     * Asset data structure
     */
    private String scan_serial_no;
    private String srvc_cntr;
    private String srvc_req_no;
    private String cnt_name;
    private String phn_no;
    private String gvn_date;
    private String problem;
    private String vehicle;
    private String remarks;

    /**
     * Class Constructor
     */
    public Service() {

    }

    /**
     * Setter for asset service Date Time
     */
    public void setCurrentDateTime(String date) {
        this.creation_date = date;
    }
    /**
     * Getter for Date_Time
     */
    public String getcurrentDateTime() {
        return creation_date;
    }

    /**
     * Setter for QR Scanned Serial Number
     */
    public void setScanSerialNumber(String scan_txt) {
        this.scan_serial_no = scan_txt;
    }
    /**
     * Getter for QR Scanned Serial Number
     */
    public String getscanSerialNumber() {
        return scan_serial_no;
    }

    /**
     * Setter for Service center name
     */
    public void setServiceCenter(String srvc_cntr) {
        this.srvc_cntr = srvc_cntr;
    }
    /**
     * Getter for Service center name
     */
    public String getserviceCenter() {
        return srvc_cntr;
    }

    /**
     * Setter for Service Request Number
     */
    public void setServiceRequestNumber(String srvc_req_no){
        this.srvc_req_no = srvc_req_no;
    }
    /**
     * Getter for Service Request Number
     */
    public String getserviceRequestNumber() {
        return srvc_req_no;
    }

    /**
     * Setter for Contact person name
     */
    public void setContactName(String cnt_name) {
        this.cnt_name = cnt_name;
    }
    /**
     * Getter for Contact person name
     */
    public String getcontactName() {
        return cnt_name;
    }

    /**
     * Setter for Contact person phone number
     */
    public void setPhoneNumber(String phn_no) {
        this.phn_no = phn_no;
    }
    /**
     * Getter for Contact person phone number
     */
    public String getphoneNumber() {
        return phn_no;
    }

    /**
     * Setter for Given date
     */
    public void setGivenDate(String gvn_date) {
        this.gvn_date = gvn_date;
    }
    /**
     * Getter for Given date
     */
    public String getgivenDate() {
        return gvn_date;
    }

    /**
     * Setter for Problems description
     */
    public void setProblemDescription(String prblm) {
        this.problem = prblm;
    }
    /**
     * Getter for Problems description
     */
    public String getproblemDescription() {
        return problem;
    }

    /**
     * Setter for Vehicle Details
     */
    public void setVehicleDetails(String vhclDtl) {
        this.vehicle = vhclDtl;
    }
    /**
     * Getter for Vehicle Details
     */
    public String getvehicleDetails() {
        return vehicle;
    }

    /**
     * Setter for Any Remarks
     */
    public void setRemark(String remark) {
        this.remarks = remark;
    }
    /**
     * Getter for Any Remarks
     */
    public String getremarks() {
        return remarks;
    }

}
