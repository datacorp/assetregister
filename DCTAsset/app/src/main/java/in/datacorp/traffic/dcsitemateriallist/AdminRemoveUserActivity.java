package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;

import in.datacorp.traffic.dcsitemateriallist.model.User;

/**
 * AdminRemoveUserActivity used to remove users from the users-tree
 */
public class AdminRemoveUserActivity extends AppCompatActivity {

    /**
     * SearchableSpinner to list_out users
     */
    private SearchableSpinner btn_users_spinner;

    /**
     * ArrayList to store users
     */
    ArrayList<String> users= new ArrayList<>();

    /**
     * String to get selected user name
     */
    String userName;

    /**
     * Button to remove selected user from user-tree
     */
    Button btn_dltUser;

    /**
     * Database reference to remove existing users
     */
    DatabaseReference userDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        /**
         * Initializing views
         */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_remove_user);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /**
         * Getting the reference of user node
         */
        userDatabaseReference = FirebaseDatabase.getInstance().getReference("User");

        /**
         * Creating UI reference
         */
        btn_users_spinner = (SearchableSpinner) findViewById(R.id.spinner_deleteUser);

        btn_dltUser = (Button) findViewById(R.id.button_deleteUser);

        btn_dltUser.setVisibility(View.INVISIBLE);

        /**
         * Creating ArrayAdapter to store list of user from database
         */
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, users);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.setNotifyOnChange(true);
        /**
         * Setting the adapter to the spinner
         */
        btn_users_spinner.setAdapter(adapter);

        /**
         * On spinner item selection, adding an onclicklistener to btn_users_spinner
         */
        btn_users_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView,View view,int i,long l) {
                userName = (String) btn_users_spinner.getSelectedItem();
                btn_dltUser.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                btn_dltUser.setVisibility(View.INVISIBLE);
            }
        });

        /**
         * deletes user when the button is clicked,adding an onclicklistener to btn_dltUser
         */
        btn_dltUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDeleteUser();
            }
        });

    }

    /**
     * finish activity
     * @return
     */
    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }

    /**
     * adds emails to the users arrayList
     */
    @Override
    protected void onStart() {
        super.onStart();
        userDatabaseReference.addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(DataSnapshot dataSnapshot) {
               for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                   User user = postSnapshot.getValue(User.class);
                   users.add(user.getUserEmail());
               }
           }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    /**
     * finish activity when back pressed
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * remove user from the users tree
     */
    public void confirmDeleteUser() {
        new AlertDialog.Builder(this)
            .setMessage("Are you sure you want to delete?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog,int which) {
                    userDatabaseReference.addValueEventListener(new ValueEventListener() {
                        /**
                         * check the email id enterd with the existing emails,
                         * if they match, remove them from  the tree
                         * @param dataSnapshot
                         */
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                                User user1 = postSnapshot.getValue(User.class);
                                if(user1.getUserEmail()==userName){
                                    userDatabaseReference.child(postSnapshot.getKey()).removeValue();
                                    users.remove(user1.getUserEmail());
                                    btn_users_spinner.setSelection(-1);
                                    new AlertDialog.Builder(AdminRemoveUserActivity.this)
                                            .setMessage("User removed successfully!")
                                            .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                                                // do something when the button is clicked
                                                public void onClick(DialogInterface arg0,int arg1) {
                                                    AdminRemoveUserActivity.super.onBackPressed();
                                                    finish();
                                                    //close();
                                                }
                                            })
                                            .show();
                                    Toast.makeText(AdminRemoveUserActivity.this,"User Removed",Toast.LENGTH_SHORT);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog,int which) {

                }
            })
            .show();

    }
}
