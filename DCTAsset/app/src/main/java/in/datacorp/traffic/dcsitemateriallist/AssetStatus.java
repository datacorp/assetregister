package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.datacorp.traffic.dcsitemateriallist.model.Register;

/**
 * This class used to show the current status of asset by scanning unique asset_id
 */
public class AssetStatus extends AppCompatActivity {

    /**
     * TableLayout to display the tble_lyt
     */
    private TableLayout tble_lyt;

    /**
     * IntentIntegrator for qr scanner
     */
    private IntentIntegrator qrScan;

    /**
     * TextView to store qr scanned text
     */
    private TextView scanText;

    /*
     * Button for viewItem
     */
    private Button viewItem;

    /**
     * ListView to show asset details
     */
    ListView listView;

    /**
     * Integer for invalidScanID
     */
    int invalidScanID;

    /**
     * List to store register_list
     */
    private List<Register> register_list = new ArrayList<>();

    /**
     * ArrayList to store transfer_list data
     */
    ArrayList<HashMap<String,String>> trans_list;

    /**
     * Database reference to get asset records
     */
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.asset_status);

        /**
         * Getting the reference of asset records
         */
        databaseReference = FirebaseDatabase.getInstance().getReference("Asset_Install");

        /**
         * Creating UI reference
         */
        trans_list = new ArrayList<HashMap<String, String>>();

        tble_lyt = (TableLayout)findViewById(R.id.table2);

        listView = (ListView) findViewById(R.id.listView);

        qrScan = new IntentIntegrator(this);

        scanText = (TextView)findViewById(R.id.scanText);

        viewItem = (Button)findViewById(R.id.showItem);

        tble_lyt.setVisibility(View.INVISIBLE);

        scanText.setVisibility(View.INVISIBLE);

        viewItem.setVisibility(View.INVISIBLE);

        /**
         * Adding an OnClickListener to buttonScan
         */
        ((Button) findViewById(R.id.buttonScan)).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                qrScan.initiateScan();
            }
        });

        /**
         * Adding an OnClickListener to saveItems button
         */
        viewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (final DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                            invalidScanID = 0;

                            String scanAddress = scanText.getText().toString();

                            Register register = postSnapshot.getValue(Register.class);
                            assert register != null;

                            Log.d("SavedQR ","" + register.getscanSerialNumber());
                            if (register.getscanSerialNumber().contentEquals(scanAddress)) {

                                register_list.clear();
                                tble_lyt.setVisibility(View.VISIBLE);
                                register_list.add(register);
                                /**
                                 * Creating data model for record and setting into adapter view
                                 */
                                RecordList record = new RecordList(AssetStatus.this,register_list);
                                /**
                                 * Attaching adapter to the listView
                                 */
                                listView.setAdapter(record);

                                /**
                                 * After successful completion of asset, set the textview as empty
                                 */
                                scanText.setText("");
                                viewItem.setVisibility(View.INVISIBLE);
                                break;
                            }
                            else {
                                //If we declare toast message here,then repeat the toast message util forLoop ends.
                                invalidScanID = 1;
                            }
                        }
                        if(invalidScanID == 1){
                            Toast.makeText(AssetStatus.this, "ERROR: Scanned asset not registered", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    /**
     * Getting the QR Scan results
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data
                try {
                    //converting the data to json
                    JSONObject obj = new JSONObject(result.getContents());
                    //setting values to textviews

                } catch (JSONException e) {
                    e.printStackTrace();
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                    scanText.setText(result.getContents());
                    scanText.setVisibility(View.VISIBLE);
                    viewItem.setVisibility(View.VISIBLE);
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
