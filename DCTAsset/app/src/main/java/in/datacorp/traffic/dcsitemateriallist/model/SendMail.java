package in.datacorp.traffic.dcsitemateriallist.model;

/**
 * Import library
 */
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * This model class extending AsyncTask to perform a networking operation
 */
public class SendMail extends AsyncTask<Void,Void,Void> {

    private static final String EMAIL ="hari97.rk@gmail.com";
    private static final String PASSWORD ="rk5415425";

    /**
     * SendMail data structure
     */
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private String email;
    private String subject;
    private String message;
    private ProgressDialog progressDialog;

    /**
     * Class Constructor
     */
    public SendMail(Context context,String email,String subject,String message){
        this.context = (Context) context;
        this.email = email;
        this.subject = subject;
        this.message = message;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //Showing progress dialog while sending email
        progressDialog = ProgressDialog.show(context,"Sending notification","Please wait...",false,false);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        //Dismissing the progress dialog
        progressDialog.dismiss();
        Toast.makeText(context,"Notification Message Sent",Toast.LENGTH_LONG).show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        Properties props = new Properties();    //Creating properties

        /**
         * Configuring properties for gmail
         */
        //If you are not using gmail you may need to change the values
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");  //Use sort port 587 if needed
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        /**
         * Creating a new session
         */
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    //Authenticating the password
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(EMAIL,PASSWORD);
                    }
                });

        try {
            /**
             * Creating MimeMessage object
             */
            MimeMessage mm = new MimeMessage(session);
            /**
             * Setting sender address
             */
            mm.setFrom(new InternetAddress(EMAIL));
            /**
             * Adding receiver, subject, message
             */
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            mm.setSubject(subject);
            mm.setText(message);

            Transport.send(mm);                 //Sending email

            Log.d("Mail--","sended");
        } catch (MessagingException e) {
            e.printStackTrace();
            //Check whether less secure app is enabled in your gmail
            // error javax.mail.AuthenticationFailedException will be display
            Log.d("MailError",""+e);
        }
        return null;
    }

}
