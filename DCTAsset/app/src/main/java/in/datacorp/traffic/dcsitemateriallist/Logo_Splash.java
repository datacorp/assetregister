package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import in.datacorp.traffic.dcsitemateriallist.model.User;

/**
 * Logo_Splash, this is starting class of the application
 */
public class Logo_Splash extends Activity {

    /**
     * Database reference to check whether user registered by admin
     */
    DatabaseReference db_reference;

    /**
     * FirebaseAuth user sync
     */
    private FirebaseAuth auth;

    /**
     * ProgressBar used to load, util syncing of data
     */
    ProgressBar viewProgressBar;

    /**
     * TextView to show network error
     */
    TextView ntwrk_err;

    /**
     * Button to Retry, if network fails
     */
    Button btn_retry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.logo_splash);

        /**
         * Creating UI reference
         */
        viewProgressBar = (ProgressBar)findViewById(R.id.progressBar);

        ntwrk_err = findViewById(R.id.ntwrk_connection);

        btn_retry = findViewById(R.id.retry);

        ntwrk_err.setVisibility(View.INVISIBLE);

        btn_retry.setVisibility(View.INVISIBLE);

        /**
         * Integer to get splash screen timer
         */
        int SPLASH_TIME_OUT = 100;

        new Handler().postDelayed(new Runnable() {

            /**
             *  Showing splash screen with a timer and this method execute once the timer is over
             */
            @Override
            public void run() {

                /**
                 * Getting the reference of user from user tree
                 */
                db_reference = FirebaseDatabase.getInstance().getReference("User");

                /**
                 * Firebase authentication
                 */
                auth = FirebaseAuth.getInstance();

                /**
                 * Validation to check network connection
                 */
                isNetworkConnectionAvailable();

                /**
                 * Getting current user
                 */
                if (auth.getCurrentUser() == null) {

                    startActivity(new Intent(Logo_Splash.this,LogInActivity.class));
                    // close this activity
                    finish();

                } else {

                    if ((auth.getCurrentUser().getEmail()).equals("siteadmin@gmail.com")) {

                        Intent intent_admin = new Intent(Logo_Splash.this,AdminIndexActivity.class);
                        startActivity(intent_admin);
                        finish();
                    } else {

                        db_reference.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                    User user = postSnapshot.getValue(User.class);

                                    assert user != null;
                                    if (user.getUserEmail().contentEquals(auth.getCurrentUser().getEmail())) {

                                        final String role = user.getUserRole();
                                        Log.d("Value","" + role);
                                        switch (role) {
                                            case "User": {

                                                Intent intent_user = new Intent(Logo_Splash.this,IndexActivity.class);
                                                startActivity(intent_user);
                                                Toast.makeText(getApplicationContext(),"Your Role " + user.getUserRole(),Toast.LENGTH_SHORT).show();
                                                finish();
                                                break;
                                            }
                                            case "Admin": {

                                                Intent intent = new Intent(Logo_Splash.this,AdminIndexActivity.class);
                                                startActivity(intent);
                                                Toast.makeText(getApplicationContext(),"Your Role " + user.getUserRole(),Toast.LENGTH_SHORT).show();
                                                finish();
                                                break;
                                            }
                                            case "Manager": {

                                                Intent intent = new Intent(Logo_Splash.this,ManagerActivity.class);
                                                startActivity(intent);
                                                Toast.makeText(getApplicationContext(),"Your Role " + user.getUserRole(),Toast.LENGTH_SHORT).show();
                                                finish();
                                                break;
                                            }
                                            default:

                                                Toast.makeText(getApplicationContext(),"Fail to load ",Toast.LENGTH_SHORT).show();
                                                break;

                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
            }

        },SPLASH_TIME_OUT);

        /**
         * Adding an OnClickListener to btn_retry
         */
        btn_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isNetworkConnectionAvailable();

            }
        });
    }

    /**
     * Funtion to validate network connection
     */
    public void isNetworkConnectionAvailable () {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert manager != null;
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnected();
        if (isConnected) {

            Log.d("Network","Connected");

        } else {

            viewProgressBar.setVisibility(View.INVISIBLE);
            ntwrk_err.setVisibility(View.VISIBLE);
            btn_retry.setVisibility(View.VISIBLE);

            Log.d("Network","Not Connected");
        }
    }

}
