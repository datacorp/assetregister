package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

/**
 * ServiceAssetIndex,index for service asset
 */
public class ServiceAssetIndex extends AppCompatActivity {

    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.service_asset_index);

        /**
         * Button to go ServiceAssetTransmit Activity
         */
        Button transmit = (Button)findViewById(R.id.srvc_asset_transmit);
        transmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aTransmit = new Intent(ServiceAssetIndex.this,ServiceAssetTransmit.class);
                ServiceAssetIndex.this.startActivity(aTransmit);

            }
        });

        /**
         * Button to go ServiceAssetReceived Activity
         */
        Button received = (Button) findViewById(R.id.srvc_asset_received);
        received.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aReceived = new Intent(ServiceAssetIndex.this,ServiceAssetReceived.class);
                ServiceAssetIndex.this.startActivity(aReceived);

            }
        });

        /**
         *  DrawerLayout for navigation view
         */
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.index);

        toggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.Open,R.string.Close);

        drawerLayout.addDrawerListener(toggle);

        toggle.syncState();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nv);

        /**
         * Navigation Header
         */
        View header = navigationView.getHeaderView(0);

        /**
         * Creating UI reference for navigation view
         */
        TextView tv_userEmail = header.findViewById(R.id.textView_userEmail);

        TextView tv_userRole = header.findViewById(R.id.textView_userRole);

        /**
         * TextView to show userEmail and userRole
         */
        tv_userEmail.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());

        tv_userRole.setText("Role: Admin");

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                /**
                 * Integer to get id(s)
                 */
                int id = item.getItemId();

                if (id == R.id.settings) {
                    startActivity(new Intent(ServiceAssetIndex.this,Settings.class));
                }
                else if (id == R.id.action_logout) {
                    new AlertDialog.Builder(ServiceAssetIndex.this)
                            .setMessage("Are you sure you want to log out?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                    FirebaseAuth.getInstance().signOut();
                                    startActivity(new Intent(ServiceAssetIndex.this, LogInActivity.class));

                                    finish();

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                // do something when the button is clicked
                                public void onClick(DialogInterface arg0, int arg1) {
                                }
                            })
                            .show();
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.index);
                drawer.closeDrawer(GravityCompat.START);
                return true;

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(toggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

}
