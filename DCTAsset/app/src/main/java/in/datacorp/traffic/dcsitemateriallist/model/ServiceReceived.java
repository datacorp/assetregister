package in.datacorp.traffic.dcsitemateriallist.model;

/**
 * Import library
 */
import android.util.Log;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * This is a model class for received asset from service
 */
@IgnoreExtraProperties
public class ServiceReceived {

    /**
     * Asset Install date
     */
    private String creation_date;

    /**
     * ServiceReceived data structure
     */
    private String scan_serial_no;
    private String office_location;
    private String condition;

    /**
     * Class Constructor
     */
    public ServiceReceived() {

    }

    /**
     * Setter for asset received Date Time
     */
    public void setCurrentDateTime(String date) {
        this.creation_date = date;
    }
    /**
     * Getter for Date & Time
     */
    public String getcurrentDateTime() {
        return creation_date;
    }

    /**
     * Setter for New Office Location(the place asset was created)
     */
    public void setOfficeLocation(String ofc_location) {
        this.office_location = ofc_location;
    }
    /**
     * Getter for New Office Location
     */
    public String getofficeLocation() {
        return office_location;
    }

    /**
     * Setter for Asset Condition
     */
    public void setCondition(String conditionResult) {
        this.condition = conditionResult;
    }
    /**
     * Getter for Asset Condition
     */
    public String getcondition() {
        return condition;
    }

    /**
     * Setter for QR Scanned Serial Number
     */
    public void setScanSerialNumber(String scan_txt) {
        this.scan_serial_no = scan_txt;

    }
    /**
     * Getter for QR Scanned Serial Number
     */
    public String getscanSerialNumber() {
        Log.d("Errors",""+scan_serial_no);
        return scan_serial_no;
    }

}
