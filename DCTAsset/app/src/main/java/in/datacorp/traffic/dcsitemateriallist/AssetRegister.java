package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.datacorp.traffic.dcsitemateriallist.model.Register;

import static android.R.*;

/**
 * This class used to register the asset information and stores into firebase database
 */
public class AssetRegister extends AppCompatActivity implements AdapterView.OnItemSelectedListener ,LocationListener{

    /**
     * TableLayout to display the tble_reg
     */
    TableLayout tble_reg;

    /**
     * Spinner to get Office Location
     */
    Spinner spinner;

    /**
     * LinearLayout to get scan result
     */
    LinearLayout lyt_scan_result;

    /**
     * IntentIntegrator for qr scanner
     */
    private IntentIntegrator qrScan;

    /**
     * TextView to store qr scanned text
     */
    TextView txt_scan;

    /**
     * ArrayAdapter to get office location
     */
    ArrayAdapter adapter;

    /**
     * ArrayAdapter to store office location
     */
    ArrayList<String> ofc_loc;

    /**
     * LocationManager for get exact location
     */
    LocationManager locationManager;

    /**
     * String to store lat&lon, address of location
     */
    String  locationHolder ,addressHolder;

    /**
     * TextView to display location text
     */
    TextView locationTxt;

    /**
     * TextView to get date_txt
     */
    TextView date_txt;

    /**
     * Calendar to get current date
     */
    Calendar current_date;

    /**
     * Button for btn_enter
     */
    Button btn_enter;

    private Boolean flag = true;

    /**
     * String of database path to store reference db
     */
    public static final String Database_Path = "Asset_Install";

    /**
     * Database reference to store asset records
     */
    DatabaseReference asset_install_db_ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initializing views
         */
        setContentView(R.layout.asset_register);
        Firebase.setAndroidContext(AssetRegister.this);

        /**
         * Getting the reference of asset records
         */
        asset_install_db_ref = FirebaseDatabase.getInstance().getReference(Database_Path);

        /**
         * Creating UI reference
         */
        tble_reg = (TableLayout)findViewById(R.id.tble_reg);

        locationTxt = (TextView)findViewById(R.id.txt_location);

        qrScan = new IntentIntegrator(this);

        lyt_scan_result = (LinearLayout)findViewById(R.id.lyt_scan_result);

        txt_scan = (TextView)findViewById(R.id.txt_scan);

        btn_enter = (Button)findViewById(R.id.enter);

        spinner = (Spinner) findViewById(R.id.spinnerGenres);

        date_txt = (TextView) findViewById(R.id.txt_asset_warranty_expiry_date);

        locationTxt.setVisibility(View.INVISIBLE);

        tble_reg.setVisibility(View.INVISIBLE);

        lyt_scan_result.setVisibility(View.INVISIBLE);

        /**
         * Create an ArrayAdapter using the string array and a default spinner layout
         */
        ofc_loc = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.office_location)));

        adapter = new ArrayAdapter<String>(this, layout.simple_spinner_item,ofc_loc);

        adapter.setDropDownViewResource(layout.simple_spinner_dropdown_item);  // Specify the layout to use when the list of choices appears

        spinner.setAdapter(adapter); // Display the adapter in spinner list

        spinner.setOnItemSelectedListener(this);

        /**
         * Getting CameraPermissions to store image
         */
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, 101);

        }

        /**
         * Getting LocationPermissions to track the exact location point
         */
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }

        /**
         * Date Picker for warranty expiry
         */
        ((Button) findViewById(R.id.btn_asset_warranty_expiry_date)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                current_date = Calendar.getInstance();
                int year= current_date.get(Calendar.YEAR);
                int month= current_date.get(Calendar.MONTH);
                int day= current_date.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog=new DatePickerDialog(AssetRegister.this, new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view,int year,int month,int dayOfMonth) {

                        @SuppressLint("DefaultLocale")
                        String set_date = String.format("%02d",dayOfMonth);
                        @SuppressLint("DefaultLocale")
                        String set_month = String.format("%02d",month+1);

                        date_txt.setText(set_date+"-"+(set_month)+"-"+year);

                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        /**
         * Adding an OnClickListener to buttonScan
         */
        ((Button) findViewById(R.id.buttonScan)).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //initiating the qr code scan
                qrScan.initiateScan();

            }
        });

        /**
         * Adding an OnClickListener to btn_location
         */
        ((Button)findViewById(R.id.btn_location)).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getLocation();
            }
        });

        /**
         * Adding an OnClickListener to btn_enter
         */
        btn_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /**
                 * removeAllViews function used to clear the previous data that stored in linear layout
                 */
                ((LinearLayout)findViewById(R.id.lyt_scan)).removeAllViews();

                asset_install_db_ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                            String scanAddress = txt_scan.getText().toString();

                            /**
                             * Getting data model
                             */
                            Register register = postSnapshot.getValue(Register.class);
                            assert register != null;

                            Log.d("SavedQR ","" + register.getscanSerialNumber());
                            if (register.getscanSerialNumber().contentEquals(scanAddress)) {
                                flag = false;
                                Log.d("Result","" + true);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                tble_reg.setVisibility(View.VISIBLE);
                btn_enter.setVisibility(View.INVISIBLE);
            }
        });

        /**
         * Adding an OnClickListener to submit
         */
        ((Button)findViewById(R.id.submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("flagResults","result "+ flag);
                if (flag) {

                    /**
                     * Validate Form
                     */
                    if (!validateForm()) {
                        return;
                    }

                    /**
                     * Getting current location were asset was installed
                     */
                    getLocation();

                    /**
                     * String to get office location, scan text, warranty expiry date, deviceID
                     */
                    String ofcLocation = spinner.getSelectedItem().toString();  //get spinner values
                    String scan_txt = txt_scan.getText().toString();
                    String txt_asset_warranty_expiry_date = date_txt.getText().toString().trim();
                    @SuppressLint("HardwareIds")
                    String deviceID = Settings.Secure.getString(getApplicationContext().getContentResolver(),Settings.Secure.ANDROID_ID);

                    /**
                     * getting a unique id using push().getKey() method and
                     * storing a unique id , we'll use it as the Primary Key for items
                     */
                    String asset_install_table_key = asset_install_db_ref.push().getKey();

                    /**
                     * Set Current Date & Time as the asset register time
                     */
                    @SuppressLint("SimpleDateFormat")
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
                    final String install_date = sdf.format(new Date());

                    /**
                     * Creating data model for new_asset
                     */
                    Register new_asset = new Register();

                    new_asset.setInstallDateTime(install_date);
                    new_asset.setTableID(asset_install_table_key);
                    new_asset.setDeviceID(deviceID);
                    new_asset.setCurrentLocation(locationHolder);

                    if(addressHolder.equals("")){
                        new_asset.setCurrentAddress("Address not scanned");
                    }
                    else {
                        new_asset.setCurrentAddress(addressHolder);
                    }

                    new_asset.setOfficeLocation(ofcLocation);
                    new_asset.setScanSerialNumber(scan_txt);
                    new_asset.setAssetType(getEditTextData(R.id.txt_asset_type));
                    new_asset.setWorkStation(getEditTextData(R.id.txt_workstation));
                    new_asset.setAssetName(getEditTextData(R.id.txt_asset_name));
                    new_asset.setSerialNumber(getEditTextData(R.id.txt_asset_serialNo));
                    new_asset.setDctID(getEditTextData(R.id.txt_asset_dctID));
                    new_asset.setOwnerName(getEditTextData(R.id.txt_asset_owner));
                    new_asset.setWarrantyDetails(getEditTextData(R.id.txt_asset_warranty_details));
                    new_asset.setWarrantExpiryDate(txt_asset_warranty_expiry_date);
                    new_asset.setVendorName(getEditTextData(R.id.txt_asset_vendor_name));
                    new_asset.setRemark(getEditTextData(R.id.txt_asset_remarks));

                    assert asset_install_table_key != null;
                    asset_install_db_ref.child(asset_install_table_key).setValue(new_asset);
                    Firebase.setAndroidContext(getApplicationContext());

                    /**
                     * After successful completion of asset, set the textview as empty
                     */
                    txt_scan.setText("");
                    locationTxt.setText("");

                    finish(); //used to close this activity page

                    startActivity(new Intent(AssetRegister.this,LastPage.class));

                    /**
                     * Showing Toast message after successfully data submit.
                     */
                    Toast.makeText(AssetRegister.this,"Asset Registered Successfully",Toast.LENGTH_LONG).show();

                }
                else{
                    btn_enter.setVisibility(View.INVISIBLE);
                    tble_reg.setVisibility(View.INVISIBLE);

                    final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(AssetRegister.this);
                    builder.setTitle("Error");
                    builder.setMessage("Asset already registered....\n Please enter new one");
                    builder.setPositiveButton("Back to Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            finish();
                            startActivity(new Intent(AssetRegister.this,AssetRegister.class));
                        }
                    });

                    builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            finish();
                            startActivity(new Intent(AssetRegister.this,IndexActivity.class));
                        }
                    });

                    android.support.v7.app.AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
    }

    private String getEditTextData(int id) {

        /**
         * Obtain the Edit widget
         */
        EditText et = (EditText)findViewById(id);
        return et.getText().toString().trim();
    }

    /**
     * Getting the QR Scan results
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data
                try {
                    //converting the data to json
                    JSONObject obj = new JSONObject(result.getContents());
                    //setting values to textviews

                } catch (JSONException e) {
                    e.printStackTrace();
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                    txt_scan.setText(result.getContents());
                    lyt_scan_result.setVisibility(View.VISIBLE);
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * set the current values of an Location & Address
     */
    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            assert locationManager != null;
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,5000,5,(LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,5000,5,(LocationListener) this);

        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n") //Indicates that line should ignore the specified warnings for the annotated element whenever something in our code isn't optimal or may crash
    public void onLocationChanged(Location location) {

        locationTxt.setVisibility(View.VISIBLE);
        locationHolder = "Latitude: " + location.getLatitude() + ", Longitude: " + location.getLongitude(); //To save location (lat/lon) in FD
        locationTxt.setText("Latitude: " + location.getLatitude() + " Longitude: " + location.getLongitude()); //Display the location

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            locationTxt.setText(locationTxt.getText() + "\n"
                    +addresses.get(0).getAddressLine(0)+", "
                    +addresses.get(0).getAddressLine(1)+", "
                    +addresses.get(0).getAddressLine(2)); //Display the address
            addressHolder = ""+addresses.get(0).getAddressLine(0); //To save address in FD

        }
        catch(Exception ignored)
        {

        }
    }

    public void onProviderDisabled(String provider) {
        Toast.makeText(AssetRegister.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();

    }

    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    public void onProviderEnabled(String provider) {

    }

    /**
     * Validation Form
     */
    private boolean validateForm() {

        boolean result = validateEditText(R.id.txt_asset_name, R.id.txt_lyt_asset_name, R.string.err_msg_required);
        result &= validateEditText(R.id.txt_asset_serialNo, R.id.txt_lyt_asset_serialNo, R.string.err_msg_required);
        result &= validateEditText(R.id.txt_asset_dctID, R.id.txt_lyt_asset_dctID, R.string.err_msg_required);
        result &= validateEditText(R.id.txt_asset_owner, R.id.txt_lyt_asset_owner, R.string.err_msg_required);
        result &= validateEditText(R.id.txt_asset_vendor_name, R.id.txt_lyt_asset_vendor_name, R.string.err_msg_required);
//        result &= validateEditText(R.id.txt_asset_warranty_details, R.id.txt_lyt_asset_warranty_details, R.string.err_msg_required);
//        result &= validateTextView(R.id.txt_asset_warranty_expiry_date, R.string.err_msg_required);
        result &= validateTextView(R.id.txt_location, R.string.get_location);
        result &= validateTextView(R.id.txt_scan, R.string.err_msg_required);

        return result;
    }

    private boolean validateEditText(int id, int text_layout_id, int error_message_res_id) {

        /**
         * Obtain the Edit widget
         */
        EditText et = (EditText)findViewById(id);
        TextInputLayout txtlyt = (TextInputLayout)findViewById(text_layout_id);

        /**
         * Validate if it is empty
         */
        if (et.getText().toString().trim().isEmpty()) {
            et.setError(getString(error_message_res_id));
            txtlyt.setErrorEnabled(true);
            return false;
        }
        txtlyt.setErrorEnabled(false); //Not Empty, Clear error and return success
        return true;
    }

    private boolean validateTextView(int id,int error_message_res_id) {

        /**
         * Obtain the Edit widget
         */
        TextView txt =(TextView)findViewById(id);
        /**
         * Validate button not pressed
         */
        if(txt.getText().toString().trim().isEmpty()){
            txt_scan.setVisibility(View.VISIBLE);
            locationTxt.setVisibility(View.VISIBLE);
            txt.setError(getString(error_message_res_id));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setMessage("Are you sure to cancel?")
            .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                // do something when the button is clicked
                public void onClick(DialogInterface arg0,int arg1) {
                    AssetRegister.super.onBackPressed();
                    finish();
                    //close();
                }
            })
            .setNegativeButton("No",new DialogInterface.OnClickListener() {

                // do something when the button is clicked
                public void onClick(DialogInterface arg0,int arg1) {
                }
            })
            .show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onItemSelected(final AdapterView<?> adapterView,final View view,int postion,long l) {
        if (postion==4) {
            // Set an EditText view to get user input
            final EditText input = new EditText(this);
            new AlertDialog.Builder(this)
                .setMessage("Enter your site location")
                .setView(input)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    public void onClick(DialogInterface dialog,int whichButton) {
                        Editable editable = input.getText();
                        String newString = editable.toString();

                        ofc_loc.clear();
                        ofc_loc.add(newString);
                        adapter.notifyDataSetChanged();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Do nothing.
                    }
                }).show();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}
