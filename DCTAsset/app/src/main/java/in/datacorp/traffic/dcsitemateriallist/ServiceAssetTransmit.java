package in.datacorp.traffic.dcsitemateriallist;

/**
 * Import library
 */
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import in.datacorp.traffic.dcsitemateriallist.model.Service;

/**
 * This activity used to store the details of asset transmit to service center
 */
public class ServiceAssetTransmit extends AppCompatActivity{

    /**
     * String of database path to store reference db
     */
    public static final String Database_Path = "Asset_Servicing";

    /**
     * Spinner to get Office Location
     */
    Spinner spinner;

    /**
     * IntentIntegrator for qr scanner
     */
    private IntentIntegrator qrScan;

    /**
     * TextView to store qr scanned text
     */
    TextView txt_scan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_asset_transmit);
        Firebase.setAndroidContext(ServiceAssetTransmit.this);

        /**
         * QR Generator
         */
        qrScan = new IntentIntegrator(this);
        txt_scan = (TextView)findViewById(R.id.txt_scan);
        txt_scan.setVisibility(View.INVISIBLE);

        /**
         * Adding an OnClickListener to buttonScan
         */
        ((Button) findViewById(R.id.buttonScan)).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //initiating the qr code scan
                qrScan.initiateScan();
                txt_scan.setVisibility(View.VISIBLE);

            }
        });

        /**
         * Adding an OnClickListener to submit
         */
        ((Button)findViewById(R.id.submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /**
                 * Validate Form
                 */
                if(!validateForm()) {
                    return;
                }

                String scan_txt = txt_scan.getText().toString();

                /**
                 * Creating DB reference for Storing the values
                 */
                DatabaseReference asset_service_db_ref = FirebaseDatabase.getInstance().getReference(Database_Path);

                /**
                 * Getting the ID from firebase database.
                 */
                String asset_service_table_key = asset_service_db_ref.push().getKey();

                /**
                 * Set Current Date & Time as the asset transmit time
                 */
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
                final String ast_transmit_date = sdf.format(new Date());

                /**
                 * Creating data model for ServiceReceived
                 */
                Service new_asset = new Service();

                new_asset.setCurrentDateTime(ast_transmit_date);
                new_asset.setScanSerialNumber(scan_txt);
                new_asset.setServiceCenter(getEditTextData(R.id.txt_srvc_cntr));
                new_asset.setServiceRequestNumber(getEditTextData(R.id.txt_srvc_req_no));
                new_asset.setContactName(getEditTextData(R.id.txt_cnt_prs_name));
                new_asset.setPhoneNumber(getEditTextData(R.id.txt_phn_no));
                new_asset.setGivenDate(getEditTextData(R.id.txt_gvn_date));

                new_asset.setProblemDescription(getEditTextData(R.id.txt_prblm));
                new_asset.setVehicleDetails(getEditTextData(R.id.txt_asset_vehicle));
                new_asset.setRemark(getEditTextData(R.id.txt_asset_remarks));

                asset_service_db_ref.child(asset_service_table_key).setValue(new_asset);
                Firebase.setAndroidContext(getApplicationContext());

                txt_scan.setText("");
                ((EditText)findViewById(R.id.txt_srvc_cntr)).setText("");
                ((EditText)findViewById(R.id.txt_srvc_req_no)).setText("");
                ((EditText)findViewById(R.id.txt_cnt_prs_name)).setText("");
                ((EditText)findViewById(R.id.txt_phn_no)).setText("");
                ((EditText)findViewById(R.id.txt_gvn_date)).setText("");
                ((EditText)findViewById(R.id.txt_prblm)).setText("");
                ((EditText)findViewById(R.id.txt_asset_vehicle)).setText("");
                ((EditText)findViewById(R.id.txt_asset_remarks)).setText("");

                /**
                 * Showing Toast message after successfully data submit.
                 */
                Toast.makeText(ServiceAssetTransmit.this,"Registered Successfully", Toast.LENGTH_LONG).show();

            }
        });
    }

    /**
     * Getting the QR Scan results
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();

            } else {
                //if qr contains data
                try {
                    //converting the data to json
                    JSONObject obj = new JSONObject(result.getContents());
                    //setting values to textviews

                } catch (JSONException e) {
                    e.printStackTrace();
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                    txt_scan.setText(result.getContents());

                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private String getEditTextData(int id) {

        /**
         * Obtain the Edit widget
         */
        EditText et = (EditText)findViewById(id);
        return et.getText().toString().trim();
    }

    /**
     * Validation Form
     */
    private boolean validateForm() {

        boolean result = validateEditText(R.id.txt_srvc_cntr, R.id.txt_lyt_srvc_cntr, R.string.err_msg_required);
        result &= validateEditText(R.id.txt_srvc_req_no, R.id.txt_lyt_srvc_req_no, R.string.err_msg_required);
        result &= validateEditText(R.id.txt_cnt_prs_name, R.id.txt_lyt_cnt_prs_name, R.string.err_msg_required);
        result &= validateEditText(R.id.txt_phn_no, R.id.txt_lyt_phn_no, R.string.err_msg_required);
        result &= validateEditText(R.id.txt_gvn_date, R.id.txt_lyt_gvn_date, R.string.err_msg_required);
        result &= validateEditText(R.id.txt_prblm, R.id.txt_lyt_prblm, R.string.err_msg_required);
        result &= validateTextView(R.id.txt_scan, R.string.err_msg_required);

        return result;
    }

    private boolean validateEditText(int id, int text_layout_id, int error_message_res_id) {

        /**
         * Obtain the Edit widget
         */
        EditText et = (EditText)findViewById(id);
        TextInputLayout txtlyt = (TextInputLayout)findViewById(text_layout_id);

        /**
         * Validate if it is empty
         */
        if (et.getText().toString().trim().isEmpty()) {
            et.setError(getString(error_message_res_id));
            txtlyt.setErrorEnabled(true);
            return false;
        }

        /**
         * Not Empty, Clear error and return success
         */
        txtlyt.setErrorEnabled(false);
        return true;
    }

    private boolean validateTextView(int id,int error_message_res_id) {

        /**
         * Obtain the Edit widget
         */
        TextView txt =(TextView)findViewById(id);
        /**
         * Validate button not pressed
         */
        if(txt.getText().toString().trim().isEmpty()){
            txt_scan.setVisibility(View.VISIBLE);
            txt.setError(getString(error_message_res_id));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setMessage("Are you sure to cancel?")
            .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                // do something when the button is clicked
                public void onClick(DialogInterface arg0,int arg1) {
                    ServiceAssetTransmit.super.onBackPressed();
                    finish();
                    //close();
                }
            })
            .setNegativeButton("No",new DialogInterface.OnClickListener() {

                // do something when the button is clicked
                public void onClick(DialogInterface arg0,int arg1) {
                }
            })
            .show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
