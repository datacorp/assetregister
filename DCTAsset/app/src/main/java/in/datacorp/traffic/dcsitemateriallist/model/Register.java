package in.datacorp.traffic.dcsitemateriallist.model;

/**
 * Import library
 */
import android.util.Log;
import java.util.Map;

/**
 * This is a model class for asset registration that contains date, table_id, device_id, location, address, office_location, scan_serial_no, asset_type, workstation, asset_name,
 *                  serial_no, dct_id, owner_name, vendor_name, warranty, warrantyExpiryDate, remarks
 */
public class Register {

    /**
     * Asset date & time
     */
    private String creation_date;

    /**
     * Asset data structure
     */
    private String table_id;
    private String device_id;
    private String location;
    private String address;
    private String office_location;
    private String scan_serial_no;
    private String asset_type;
    private String workstation;
    private String asset_name;
    private String serial_no;
    private String dct_id;
    private String owner_name;
    private String vendor_name;
    private String warranty;
    private String warrantyExpiryDate;
    private String remarks;

    private Map<String, Object> asset_transfer_records;

    /**
     * Class Constructor
     */
    public Register() {

    }

    /**
     * Setter for Install Date Time
     */
    public void setInstallDateTime(String date) {
        this.creation_date = date;
    }
    /**
     * Getter for Date & Time
     */
    public String getinstallDateTime() {
        return creation_date;
    }

    /**
     * Setter for Table ID
     */
    public void setTableID(String table_id) {
        this.table_id = table_id;
    }
    /**
     * Getter for TableID
     */
    public String getTableID() {
        return table_id;
    }

    /**
     * Setter for Device ID
     */
    public void setDeviceID(String device_id) {
        this.device_id = device_id;
    }
    /**
     * Getter for Device ID
     */
    public String getdeviceID() {
        return device_id;
    }

    /**
     * Setter for Current Location (latitude and longitude)
     */
    public void setCurrentLocation(String loc) {
        location = loc;
    }
    /**
     * Getter for Current Location
     */
    public String getcurrentLocation() {
        return location;
    }

    /**
     * Setter for Current Address
     */
    public void setCurrentAddress(String add) {
        address = add;
    }
    /**
     * Getter for Current Address
     */
    public String getcurrentAddress() {
        return address;
    }

    /**
     * Setter for Office Location(the place where asset was created)
     */
    public void setOfficeLocation(String ofc_location) {
        this.office_location = ofc_location;

    }
    /**
     * Getter for Office location
     */
    public String getofficeLocation() {
        return office_location;
    }

    /**
     * Setter for QR Scanned Serial Number
     */
    public void setScanSerialNumber(String scan_txt) {
        this.scan_serial_no = scan_txt;
    }
    /**
     * Getter for QR Scanned Serial Number
     */
    public String getscanSerialNumber() {
        return scan_serial_no;
    }

    /**
     * Setter for Asset Type
     */
    public void setAssetType(String asset_type) {
        this.asset_type = asset_type;
    }
    /**
     * Getter for Asset Type
     */
    public String getassetType() {
        return asset_type;
    }

    /**
     * Setter for Asset WorkStation
     */
    public void setWorkStation(String workstation) {
        this.workstation = workstation;
    }
    /**
     * Getter for Asset WorkStation
     */
    public String getworkStation() {
        return workstation;
    }

    /**
     * Setter for Asset Name
     */
    public void setAssetName(String asset_name) {
        this.asset_name = asset_name;
    }
    /**
     * Getter for Asset Name
     */
    public String getassetName() {
        return asset_name;
    }

    /**
     * Setter for Asset Serial.No
     */
    public void setSerialNumber(String serial_no) {
        this.serial_no = serial_no;
    }
    /**
     * Getter for Asset Serial.No
     */
    public String getserialNumber() {
        return serial_no;
    }

    /**
     * Setter for Asset DataCorp ID
     */
    public void setDctID(String dct_id) {
        this.dct_id = dct_id;
    }
    /**
     * Getter for Asset DataCorp ID
     */
    public String getdctID() {
        return dct_id;
    }

    /**
     * Setter for Owner Name
     */
    public void setOwnerName(String owner_name) {
        this.owner_name = owner_name;
    }
    /**
     * Getter for Owner Name
     */
    public String getownerName() {
        return owner_name;
    }

    /**
     * Setter for Vendor Name
     */
    public void setVendorName(String vendor_name) {
        this.vendor_name = vendor_name;
    }
    /**
     * Getter for Vendor Name
     */
    public String getvendorName() {
        return vendor_name;
    }

    /**
     * Setter for Asset Warranty Details
     */
    public void setWarrantyDetails(String warranty) {
        this.warranty = warranty;
    }
    /**
     * Getter for Asset Warranty Details
     */
    public String getwarrantyDetails() {
        return warranty;
    }

    /*
     * Setter for Asset Warranty Expiry Date
     */
    public void setWarrantExpiryDate(String expiry_date) {
        this.warrantyExpiryDate = expiry_date;
    }
    /**
     * Getter for Asset Waranty Expiry Date
     */
    public String getwarrantyExpiryDate() {
        return warrantyExpiryDate;
    }

    /**
     * Setter for Any Remarks
     */
    public void setRemark(String remark) {
        this.remarks = remark;
    }
    /**
     * Getter for Any Remarks
     */
    public String getremarks() {
        return remarks;
    }

    /**
     * Setter for Date Records
     */
    public void setAsset_transfer_records(Map<String, Object> asset_transfer_records) {
        this.asset_transfer_records = asset_transfer_records;
    }
    /**
     * Getter for Date Records
     */
    public Map<String, Object> getasset_transfer_records() {
        Log.d("Records",""+ asset_transfer_records);
        return asset_transfer_records;
    }

}
